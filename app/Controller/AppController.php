<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
 	public $components = array(
	    'Auth' => array(
	        'loginAction' => array(
	            'controller' => 'users',
	            'action' => 'login'
	        ),
	        'authError' => 'Did you really think you are allowed to see that?',
	        'authenticate' => array(
	            'Form' => array(
	                'fields' => array(
	                  'username' => 'email', //Default is 'username' in the userModel
	                  'password' => 'password'  //Default is 'password' in the userModel
	                )
	            )
	        )
	    ),'RequestHandler','Session'
	);
	public $ImagemenuPath = "uploads/menuitem/";
	public $ImagelargePath = "uploads/gallery/large/";
	public $ImagesmallPath = "uploads/gallery/small/";
	// public $menuCategory = array(""=>"~ Select Category ~", "1" => "Breakfast", "2" => "Lunch", "3" => "Dinner");
	public $default_limit = 20;
	public $boolenStatus = array( "1" => "Active", "0" => "Inactive");
	 public function beforeFilter(){
	 	$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login','admin'=>true);
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login','admin'=>true);
		$this->Auth->loginRedirect = array('controller' => 'pages', 'action' => 'dashboard','admin'=>true);
		// $this->Auth->loginRedirect =  array('controller' => 'users', 'action' => 'login','admin'=>true);

	 	$this->set('ImagemenuPath', $this->ImagemenuPath);

 		$this->set('ImagelargePath', $this->ImagelargePath);
 		$this->set('ImagesmallPath', $this->ImagesmallPath);

	 	$this->set('default_limit', $this->default_limit);
	 	$this->set('boolenStatus', $this->boolenStatus);
	 }
	 public function imagename($name){
      if($name != ''){
          $newName=rand(0,9999999).'_'.$name;
          $newName= preg_replace('#[ -]+#', '-', preg_replace('/[%()]/', '', preg_replace('/[^a-zA-Z0-9.]+/', ' ', $newName)));
          return $newName;
      }
    }
	public function datetimeToDb($date= null){
		if(!empty($date)){
			$date = date("Y-m-d h:i:s", strtotime(str_replace('-',' ',$date)));
			return $date;
		}else{
			return null;
		}
	}
}
