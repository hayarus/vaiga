<?php
App::uses('AppController', 'Controller');
/**
 * Galleries Controller
 *
 * @property Gallery $Gallery
 * @property PaginatorComponent $Paginator
 */
class GalleriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','ImageCakeCut');


/**
 * index method
 *
 * @return void
 */
public function beforeFilter() {
		parent::beforeFilter();
		//$this->Auth->allow('getByCategory');
		$this->layout='admin_default';
	}
	public function admin_index() {
		$this->Gallery->recursive = 0;
		$this->set('galleries', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Gallery->exists($id)) {
			throw new NotFoundException(__('Invalid gallery'));
		}
		$options = array('conditions' => array('Gallery.' . $this->Gallery->primaryKey => $id));
		$this->set('gallery', $this->Gallery->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$image= $this->request->data['Gallery']['imgname']['name'];
			$imageName = 'img-'.rand(0,9999999).'-'.$image;
			
			if(move_uploaded_file($this->request->data['Gallery']['imgname']['tmp_name'], $this->ImagesmallPath.$imageName)){
					$this->ImageCakeCut->resize($this->ImagesmallPath.$imageName, $this->ImagelargePath.$imageName,'width', 750);
					$this->ImageCakeCut->resize($this->ImagesmallPath.$imageName, $this->ImagesmallPath.$imageName,'width', 370);		
			}
			
			$this->request->data['Gallery']['status']= $this->request->data['Gallery']['status'];
			$this->request->data['Gallery']['imgname']= $imageName ;
			$this->Gallery->create();
			if ($this->Gallery->save($this->request->data)) {
				$this->Session->setFlash('The gallery item has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The gallery item could not be saved. Please, try again.','flash_failure');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$ImagesmallPath=$this->ImagesmallPath;
		$ImagelargePath=$this->ImagelargePath;
		if (!$this->Gallery->exists($id)) {
			throw new NotFoundException(__('Invalid gallery'));
		}
		if ($this->request->is(array('post', 'put'))) {

			if(empty($this->request->data['Gallery']['imgname']['name']))
				unset($this->request->data['Gallery']['imgname']);
			else {
				$crntImgdetails = $this->Gallery->find('first',array('conditions'=>array('Gallery.id'=>$id)));
				$crntImg = $crntImgdetails['Gallery']['imgname'];
				@unlink($ImagesmallPath.$crntImg);
				@unlink($ImagelargePath.$crntImg);
				
				
				$image= $this->request->data['Gallery']['imgname']['name'];
				$imageName = 'img-'.rand(0,9999999).'-'.$image;
				
				if(move_uploaded_file($this->request->data['Gallery']['imgname']['tmp_name'], $ImagesmallPath.$imageName)){
						
					$this->ImageCakeCut->resize($this->ImagesmallPath.$imageName, $this->ImagelargePath.$imageName,'width', 750);
					$this->ImageCakeCut->resize($this->ImagesmallPath.$imageName, $this->ImagesmallPath.$imageName,'width', 370);	
				}
				
				$this->request->data['Gallery']['status']= $this->request->data['Gallery']['status'];
				$this->request->data['Gallery']['imgname']= $imageName ;
			}	
			if ($this->Gallery->save($this->request->data)) {
				$this->Session->setFlash('The gallery item has been saved.','flash_success');
				return $this->redirect(array('action'=>'index'));
			}
			else {
				$this->Session->setFlash('The gallery item could not be saved. Please, try again.','flash_failure');
				return $this->redirect(array('action'=>'index'));
			}
		} else {
			$options = array('conditions' => array('Gallery.' . $this->Gallery->primaryKey => $id));
			$this->request->data = $this->Gallery->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$ImagelargePath= $this->ImagelargePath;
		$ImagesmallPath= $this->ImagesmallPath;
		$this->Gallery->id = $id;
		if (!$this->Gallery->exists()) {
			throw new NotFoundException(__('Invalid gallery'));
		}
		$options= array('conditions'=>array('Gallery.id'=>$id));
		$arrImage= $this->Gallery->find('first',$options);
		$this->request->allowMethod('post', 'delete');
		$ImagelargePath = new File($ImagelargePath.$arrImage['Gallery']['imgname']);
		$ImagesmallPath = new File($ImagesmallPath.$arrImage['Gallery']['imgname']);

		$ImagelargePath->delete();
		$ImagesmallPath->delete();
		if ($this->Gallery->delete()) {
			$this->Session->setFlash('Image has been deleted.', 'flash_success');
				return $this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash('The Items(s) details could not be deleted. Please, try again.','flash_failure');
			}	
		
		exit;
	}
}
