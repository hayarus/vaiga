<?php
App::uses('AppController', 'Controller');
/**
 * Menucategories Controller
 *
 * @property Menucategory $Menucategory
 * @property PaginatorComponent $Paginator
 */
class MenucategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function beforeFilter() { 
		parent::beforeFilter();
		//$this->Auth->allow('getByCategory');
		$this->layout='admin_default';
	}
	public function admin_index() {
		$this->Menucategory->recursive = 0;
		$this->set('menucategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Menucategory->exists($id)) {
			throw new NotFoundException(__('Invalid Menu Category'));
		}
		$options = array('conditions' => array('Menucategory.' . $this->Menucategory->primaryKey => $id));
		$this->set('menucategory', $this->Menucategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Menucategory->create();
			if ($this->Menucategory->save($this->request->data)) {
				$this->Session->setFlash('The Menu category has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The Menu category could not be saved. Please, try again.','flash_failure');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Menucategory->exists($id)) {
			throw new NotFoundException(__('Invalid Menu Category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Menucategory->save($this->request->data)) {
				$this->Session->setFlash('The Menu category has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The Menu category could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Menucategory.' . $this->Menucategory->primaryKey => $id));
			$this->request->data = $this->Menucategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Menucategory->id = $id;
		if (!$this->Menucategory->exists()) {
			throw new NotFoundException(__('Invalid Menu Category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Menucategory->delete()) {
			$this->Session->setFlash('The Menu category has been deleted.','flash_success');
		} else {
			$this->Session->setFlash('The Menu category could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
