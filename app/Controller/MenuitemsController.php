<?php
App::uses('AppController', 'Controller');
/**
 * Menuitems Controller
 *
 * @property Menuitem $Menuitem
 * @property PaginatorComponent $Paginator
 */
class MenuitemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','ImageCakeCut');
	public function beforeFilter() { 
		parent::beforeFilter();
		$this->Auth->allow('menuitems');
		$this->layout='admin_default';
	}
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Menuitem']['limit'])){
            	$limit = $this->data['Menuitem']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Menuitem.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Menuitem.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Menuitem->recursive = 0;
		$this->set('menuitems', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Menuitem->exists($id)) {
			throw new NotFoundException(__('Invalid menuitem'));
		}
		$options = array('conditions' => array('Menuitem.' . $this->Menuitem->primaryKey => $id));
		$this->set('menuitem', $this->Menuitem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$image= $this->request->data['Menuitem']['imgname']['name'];
			$imageName = 'img-'.rand(0,9999999).'-'.$image;
			
			if(move_uploaded_file($this->request->data['Menuitem']['imgname']['tmp_name'], $this->ImagemenuPath.$imageName)){
				$this->ImageCakeCut->resize($this->ImagemenuPath.$imageName, $this->ImagemenuPath.$imageName,'width', 120);	
			}
			
			$this->request->data['Menuitem']['category']= $this->request->data['Menuitem']['category'];
			$this->request->data['Menuitem']['imgname']= $imageName ;
			$this->Menuitem->create();
			if ($this->Menuitem->save($this->request->data)) {
				$this->Session->setFlash('The menu item has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The menu item could not be saved. Please, try again.','flash_failure');
			}
		}
		$menucategories = $this->Menuitem->Menucategory->find('list');
		$this->set(compact('menucategories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$ImagemenuPath=$this->ImagemenuPath;
		if (!$this->Menuitem->exists($id)) {
			throw new NotFoundException(__('Invalid menuitem'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if(empty($this->request->data['Menuitem']['imgname']['name']))
				unset($this->request->data['Menuitem']['imgname']);
			else {
				$crntImgdetails = $this->Menuitem->find('first',array('conditions'=>array('Menuitem.id'=>$id)));
				$crntImg = $crntImgdetails['Menuitem']['imgname'];
				@unlink($ImagemenuPath.$crntImg);
				@unlink($ImagemenuPath.$crntImg);
				
				
				$image= $this->request->data['Menuitem']['imgname']['name'];
				$imageName = 'img-'.rand(0,9999999).'-'.$image;
				
				if(move_uploaded_file($this->request->data['Menuitem']['imgname']['tmp_name'], $ImagemenuPath.$imageName)){
						
						$this->ImageCakeCut->resize($ImagemenuPath.$imageName, $ImagemenuPath.$imageName,'width', 120);
						
						
					
				}
				
				$this->request->data['Menuitem']['category']= $this->request->data['Menuitem']['category'];
				$this->request->data['Menuitem']['imgname']= $imageName ;
			}	
			if ($this->Menuitem->save($this->request->data)) {
				$this->Session->setFlash('The menu item has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The menu  item could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Menuitem.' . $this->Menuitem->primaryKey => $id));
			$this->request->data = $this->Menuitem->find('first', $options);
		}
		$menucategories = $this->Menuitem->Menucategory->find('list');
		$this->set(compact('menucategories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$ImagemenuPath= $this->ImagemenuPath;
		$this->Menuitem->id = $id;
		if (!$this->Menuitem->exists()) {
			throw new NotFoundException(__('Invalid menuitem'));
		}
		$options= array('conditions'=>array('Menuitem.id'=>$id));
		$arrImage= $this->Menuitem->find('first',$options);
		$this->request->allowMethod('post', 'delete');
		$ImagemenuPath = new File($ImagemenuPath.$arrImage['Menuitem']['imgname']);

		$ImagemenuPath->delete();
		
		if ($this->Menuitem->delete()) {
			$this->Session->setFlash('Menuitem has been deleted.', 'flash_success');
				return $this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash('The Items(s) details could not be deleted. Please, try again.','flash_failure');
			}	
		
		exit;
	}

	
}

	