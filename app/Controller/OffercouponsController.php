<?php
App::uses('AppController', 'Controller');
/**
 * Offercoupons Controller
 *
 * @property Offercoupon $Offercoupon
 * @property PaginatorComponent $Paginator
 */
class OffercouponsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		$this->Auth->allow('admin_getemail');
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Offercoupon']['limit'])){
            	$limit = $this->data['Offercoupon']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Offercoupon.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Offercoupon.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Offercoupon->recursive = 0;
		$this->set('offercoupons', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function admin_getemail(){
		$this->autoRender = false;
		if ($this->request->is(['post'])) {
        $email =$this->request->data('email');                                        
        $query = $this->Offercoupon->find('all',array('conditions'=>array('Offercoupon.email '=>$email)));
        if(!empty($query)){
             echo 1;
        }
    	}
	}

	public function admin_view($id = null) {
		if (!$this->Offercoupon->exists($id)) {
			throw new NotFoundException(__('Invalid offercoupon'));
		}
		$options = array('conditions' => array('Offercoupon.' . $this->Offercoupon->primaryKey => $id));
		$this->set('offercoupon', $this->Offercoupon->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$coupon='C - '.rand(0,99999);
		if ($this->request->is('post')) {

			$name=$this->data['Offercoupon']['name'];
			$email=$this->data['Offercoupon']['email'];
			$phone=$this->data['Offercoupon']['phone'];
			$coupon_No=$this->data['Offercoupon']['coupon'];
			
			$admin_email="jijutom.wst@gmail.com";
			$subject="Special offer coupon";
			$fromemail=$admin_email;
		    $to = $email;
		    
		    $restaurant="Kayal Kerala Cusine";
			$txt = '<html><body>';
			$txt .="
			Hi ". $name .",
			<br/><br/>

		   	<table> 
		   	<tr><td>Subject   	 : $subject</td></tr>
		   	<tr><td>Coupon No: $coupon_No</td></tr>
		   	<tr><td></td></tr>
		    </table>
			</br></br><br/><br style='clear:both'/>
		    Best Regards,<br/>".$restaurant . "
		    ";
			$txt .= "</body></html>";
	        
			$headers = "From: ".$restaurant." <" . $fromemail. ">\r\n";
			$headers .= "Reply-To: ".$fromemail."\r\n";
			
			//$headers .='X-Mailer: PHP/' . phpversion();
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

			$this->Offercoupon->create();
			$this->Offercoupon->save($this->request->data);

			if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
			{
				$this->Session->setFlash('The offer coupon has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Offercoupon->id));

			}else{

			    $this->Session->setFlash('The offer coupon could not be added. Please, try again.','flash_failure');
		        
			}
		}
		$this->set(compact('coupon'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Offercoupon->exists($id)) {
			throw new NotFoundException(__('Invalid offercoupon'));
		}
		if ($this->request->is(array('post', 'put'))) {

			if ($this->Offercoupon->save($this->request->data)) {
				$this->Session->setFlash('The offer coupon has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Offercoupon->id));
			} else {
				$this->Session->setFlash('The offer coupon could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Offercoupon.' . $this->Offercoupon->primaryKey => $id));
			$this->request->data = $this->Offercoupon->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Offercoupon->id = $id;
		if (!$this->Offercoupon->exists()) {
			throw new NotFoundException(__('Invalid offer coupon'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Offercoupon->delete()) {
			$this->Session->setFlash('The offer coupon has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The offer coupon could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

}
 