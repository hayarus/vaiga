<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->layout="front-end";
		$this->Auth->allow('index','gallery','aboutus','contact','menu','offercoupons','getemail');
	}

	public function getemail(){
		$this->autoRender = false;
		$this->loadmodel('Offercoupon');
		if ($this->request->is(['post'])) {
        $email =$this->request->data('email');                                        
        $query = $this->Offercoupon->find('all',array('conditions'=>array('Offercoupon.email '=>$email)));
        if(!empty($query)){
             echo 1;
        }
    	}
	}
	public function offercoupons(){
		$this->autoRender = false;
		$this->loadModel('Offercoupon');
		if($this->request->is('ajax')){
			if(!empty($this ->request->data['name'])){
			$this ->request->data['Offercoupon']['name']=$this ->request->data['name'];
		    $this ->request->data['Offercoupon']['email'] =$this ->request->data['emailid'];
			$this->request->data['Offercoupon']['phone']=$this->request->data['phoneno'];

			$coupon='C - '.rand(0,99999);
			$this ->request->data['Offercoupon']['coupon']=$coupon;

		    $name= $this ->request->data['Offercoupon']['name'];
		    $email= $this ->request->data['Offercoupon']['email'] ;
		    $phone= $this->request->data['Offercoupon']['phone'];

		    

			$admin_email="jijutom.wst@gmail.com";
			$subject="Special offer coupon";
			$fromemail=$admin_email;
		    $to = $email;
		    
		    $restaurant="Kayal Kerala Cusine";
			$txt = '<html><body>';
			$txt .="
			Hi ". $name .",
			<br/><br/>

		   	<table> 
		   	<tr><td>Subject   	 : $subject</td></tr>
		   	<tr><td>Coupon No: $coupon</td></tr>
		   	<tr><td></td></tr>
		    </table>
			</br></br><br/><br style='clear:both'/>
		    Best Regards,<br/>".$restaurant . "
		    ";
			$txt .= "</body></html>";
	        
			$headers = "From: ".$restaurant." <" . $fromemail. ">\r\n";
			$headers .= "Reply-To: ".$fromemail."\r\n";
			
			//$headers .='X-Mailer: PHP/' . phpversion();
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

			$this->Offercoupon->create();
			$this->Offercoupon->save($this->request->data);

			if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
			{
			    echo 1;
			}
		}
		} 
	}
	public function index() {

		$this->layout="front-end";
		$this ->loadModel('Reservation');
		if($this->request->is('post')){
			if(!empty($this ->request->data['customername'])){
			    $this ->request->data['Reservation']['customername']=$this ->request->data['customername'];
			    $this ->request->data['Reservation']['revdate'] =$this ->request->data['revdate'];
			   	if(!empty($this ->request->data['Reservation']['revdate']))
				$this ->request->data['Reservation']['revdate'] = $this->datetimeToDb($this ->request->data['Reservation']['revdate']);
				$this->request->data['Reservation']['nofperson']=$this->request->data['nofperson'];
				$this->Reservation->create();
				$this->Reservation->save($this->request->data);

			    $name=$this ->request->data['customername'];
			    $email= $this->request->data['customeremail'];
			    $date= $this->request->data['revdate'];
			    $noperson= $this->request->data['nofperson'];
			    $phone= $this->request->data['phone'];
				// $this->Reservation->save($this->request->data);
				// $this->Reservation->save( array('customername'=>$name,'date'=>$date,'nofperson'=>$noperson));
			    $subject = "New Reservation";
			    $admin_email='jijutom.wst@gmail.com';
			    
		        $fromemail=$email;
			    $to = $admin_email;
			   
				$txt = '<html><body>';
				$txt .="
				Hi Admin,
				<br/><br/>

			   	<table> 
			    <tr><td>Name: $name</td></tr>
				<tr><td>Email      	 : $email</td></tr>
				<tr><td>Subject   	 : $subject</td></tr>
				<tr><td>Date   	     : $date</td></tr>
				<tr><td>No Of Person : $noperson</td></tr>
				<tr><td>Phone        : $phone</td></tr>
			    </table>
				</br></br><br/><br style='clear:both'/>
			    Best Regards,<br/>".$name . "
			    ";
				$txt .= "</body></html>";
		        
				$headers = "From: ".$name." <" . $fromemail. ">\r\n";
				$headers .= "Reply-To: ".$fromemail."\r\n";
				
				//$headers .='X-Mailer: PHP/' . phpversion();
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 	

				
				$this->Reservation->save($this->request->data);

				if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
				{
				    $this->Session->setFlash('Thank you for contacting us. The enquiry has been received. We will get back you soon.','info_notif');
						return $this->redirect(array('controller'=>'pages','action' => 'index'));
				}else{
				    $this->Session->setFlash('Error while sending mail.','fail_flash');
			         return $this->redirect(array('controller'=>'pages','action' => 'index'));
				}
			}
			
		}

		$this->loadModel('Menuitem');
		$allmenuitems = $this->Menuitem->find('all',array('group'=>'Menuitem.name'));
		$this->set('allmenuitems', $allmenuitems);
		$breakfasts = $this->Menuitem->find('all');//,array('conditions'=>array('Menuitem.category=1'))
		$this->set('breakfasts', $breakfasts);
		$lunches = $this->Menuitem->find('all');//,array('conditions'=>array('Menuitem.category=2'))
		$this->set('lunches', $lunches);
		$dinners = $this->Menuitem->find('all');//,array('conditions'=>array('Menuitem.category=3'))
		$this->set('dinners', $dinners);

		// $this->loadModel('Gallery');
		// $images = $this->Gallery->find('all', array('limit' => 6,'conditions'=>array('Gallery.status=1')));
		// $this->set('images', $images);
   
	}
	public function aboutus() {
		$this->layout="front-end";
	}
	public function gallery() {
		$this->layout="front-end";
		$this->loadModel('Gallery');
		$images = $this->Gallery->find('all', array('conditions'=>array('Gallery.status=1')));
		$this->set('images', $images);
	}
	public function blog() {
		$this->layout="front-end";
	}
	public function blog_details() {
		$this->layout="front-end";
	}
	public function contact() {
		$this->layout="front-end";

		if($this->request->is('post')){

		    $name=$this->request->data['name'];
		    $email= $this->request->data['emailid'];
		    $subject = "Contact";
		    $message= $this->request->data['message'];
		    $admin_email='jijutom.wst@gmail.com';
		    
	        $fromemail=$email;
		    $to = $admin_email;
		   
			$txt = '<html><body>';
			$txt .="
			Hi Admin,
			<br/><br/>

		   	<table> 
		    <tr><td>Name: $name</td></tr>
			<tr><td>Email     : $email</td></tr>
			<tr><td>Message   : $message</td></tr>
		    </table>
			</br></br><br/><br style='clear:both'/>
		    Best Regards,<br/>".$name . "
		    ";
			$txt .= "</body></html>";
	        
			$headers = "From: ".$name." <" . $fromemail. ">\r\n";
			$headers .= "Reply-To: ".$fromemail."\r\n";
			
			//$headers .='X-Mailer: PHP/' . phpversion();
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 		
			if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
			{
			    $this->Session->setFlash('Thank you for contacting us. The enquiry has been received. We will get back you soon.','success');
					return $this->redirect(array('controller'=>'pages','action' => 'index'));
			}else{
			    $this->Session->setFlash('Error while sending mail.','fail_flash');
		         return $this->redirect(array('controller'=>'pages','action' => 'index'));
			}

		}
	    
	}
	public function menu() {
		$i=$this->i;
		
		// pr($i);exit;
		$this->loadModel('Menucategory');
		$menucategories=$this->Menucategory->find('all',array('conditions'=>array('Menucategory.status' => 1)));
		$this->set('menucategories', $menucategories);
		$this->loadModel('Menuitem');
		$allmenuitems = $this->Menuitem->find('all',array('group'=>'Menuitem.name'));
		$this->set('allmenuitems', $allmenuitems);
		$breads= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 1 )));
		$this->set('breads', $breads);
		$delicious= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 2 )));
		$this->set('delicious', $delicious);
		$drinks= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 3 )));
		$this->set('drinks', $drinks);
		$eggvarieties= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 4 )));
		$this->set('eggvarieties', $eggvarieties);
		$malabars= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 5 )));
		$this->set('malabars', $malabars);
		$mspecials= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 6 )));
		$this->set('mspecials', $mspecials);
		$rcorners= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 7 )));
		$this->set('rcorners', $rcorners);
		$starters= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 8 )));
		$this->set('starters', $starters);
		$seafoods= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 9 )));
		$this->set('seafoods', $seafoods);
		$vcorners= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 10 )));
		$this->set('vcorners', $vcorners);
		$weekends= $this->Menuitem->find('all',array('conditions'=>array('Menuitem.menucategory_id' => 11 )));
		$this->set('weekends', $weekends);
		}	


public function admin_dashboard() {
		$this->layout='admin_default';
	}

}