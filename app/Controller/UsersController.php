<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('admin_add');
		$this->layout='admin_default';
	}
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
			$this->Session->setFlash('The user has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash('The user could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
			$this->Session->setFlash('The user has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'admin_index', $this->User->id));
			} else {
				$this->Session->setFlash('The user could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash('The user has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The user could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'admin_index'));
	}


	public function admin_login(){
	
		$this->layout="admin_login";
		if($this->request->is('post')){//pr($this->request->data);exit;
			$this->request->data['User']['email']=$this->request->data['User']['email'];
			$this->request->data['User']['password']=$this->request->data['User']['password'];
			$this->Session->delete('Auth');
			if($this->Auth->login()){
				//echo "here";exit;
				$this->Session->delete('sessionUserInfo');
				$userInfo = $this->Auth->user();
				$this->User->id = $this->Auth->user('id');
				$this->redirect($this->Auth->redirectUrl());
				$this->Session->write("sessionUserInfo",$userInfo);
				
				
			}else{
				$this->Session->setFlash('Incorrect email or password, try again.', 'flash_failure');
				return $this->redirect($this->Auth->redirect());
			}
			//pr($this->request->data);exit;
		}	
		
	}

	public function admin_logout(){
		$this->Session->delete("sessionUserInfo");
		$this->Session->delete("Auth");
		$this->Session->destroy();
		$this->Session->setFlash('You have successfully logged out.', 'flash_success');
		//pr($this->Session->read());exit;
		$this->redirect($this->Auth->logout());
		
	}
}
