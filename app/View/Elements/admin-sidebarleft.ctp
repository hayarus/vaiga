<?php $user = $this->Session->read('Auth.User'); 
      $this->set('tabPermission', $this->Session->read("tabPermission"));
?>
<ul class="page-sidebar-menu">
<li class="sidebar-toggler-wrapper">
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler hidden-phone">
                    </div>
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                </li>
                <br>
                
                <li class=" <?php if($this->request->params['controller']=='dashboard' && ($this->request->params['action']=='index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/pages/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">
                        Dashboard
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                
                
                <li class=" <?php if($this->request->params['controller']=='menucategories' && ($this->request->params['action']=='add' || $this->request->params['action'] == 'edit' || $this->request->params['action'] == 'view' || $this->request->params['action'] == 'index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/menucategories/index">
                    <i class="fa fa-tasks"></i>
                    <span class="title">
                         Menu  Categories
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='menuitems' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/menuitems/index">
                    <i class="fa fa-th-large"></i>
                    <span class="title">
                        Menu Items
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='galleries' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/galleries/index">
                    <i class="fa fa-camera"></i>
                    <span class="title">
                        Image Gallery
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='offercoupons' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/offercoupons/index">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        Offer Coupons
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
               <!--  <li class=" <?php if($this->request->params['controller']=='trends' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/trends/index">
                    <i class="fa fa-flag"></i>
                    <span class="title">
                        New Trends
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li> -->
                <li class=" <?php if($this->request->params['controller']=='postimages' && ($this->request->params['action']=='add' || $this->request->params['action'] == 'edit' || $this->request->params['action'] == 'view' || $this->request->params['action'] == 'index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/logout">
                    <i class="fa fa-power-off"></i>
                    <span class="title">
                        Logout  
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <!-- <li class=" <?php if($this->request->params['controller']=='postvideos' && ($this->request->params['action']=='add' || $this->request->params['action'] == 'edit' || $this->request->params['action'] == 'view' || $this->request->params['action'] == 'index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/postvideos/index">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        Post Videos
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li> -->
</ul>                