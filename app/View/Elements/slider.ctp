<div class="slider-container">
<div id="mainSlider" class="nivoSlider slider-image">
                <img src="<?php echo $this->webroot;?>assets/images/slider/1.jpg" alt="" title="#htmlcaption1"/>
                <img src="<?php echo $this->webroot;?>assets/images/slider/3.jpg" alt="" title="#htmlcaption2"/>
                <img src="<?php echo $this->webroot;?>assets/images/slider/2.jpg" alt="" title="#htmlcaption3"/>
            </div> 
            <!-- Slider Caption 1 -->
            <div id="htmlcaption1" class="nivo-html-caption slider-caption-1">
                <div class="slider-text-table">
                    <div class="slider-text-tablecell">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-2 col-sm-3 hidden-xs">
                                    <div class="social-media-follow">
                                        <div class="social-box-inner">
                                            <ul>
                                                <li><a href="https://www.facebook.com/kaayalkerala/" target= "_blank"><i class="mdi mdi-facebook"></i></a></li>
                                                <!-- <li><a href="#"><i class="mdi mdi-twitter"></i></a></li>
                                                <li><a href="#"><i class="mdi mdi-dribbble"></i></a></li>
                                                <li><a href="#"><i class="mdi mdi-pinterest"></i></a></li>
                                                <li><a href="#"><i class="mdi mdi-instagram"></i></a></li> -->
                                            </ul>   
                                        </div>
                                        <p>follow on</p>
                                    </div>
                                </div>
                                <div class="col-md-10 col-sm-9 col-xs-12">
                                    <div class="slide1-text">
                                        <div class="middle-text">
                                            <div class="title-1 wow rotateInDownRight" data-wow-duration="0.9s" data-wow-delay="0s">
                                                <h2>Welcome to Kaayal Kerala Cuisine </h2>
                                            </div>  
                                            <div class="title-2 wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
                                                <h1></h1>
                                            </div>  
                                            <div class="desc wow slideInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
                                                <p>Please be welcome to share our new dining experience with our mouth watering dishes.
                                                    <br> Enjoy a pleasant dining experience with us or in the comfort of home.
                                                </p>
                                            </div>
                                            <div class="order-now wow bounceInUp" data-wow-duration="1.3s" data-wow-delay=".5s">
                                                <a href="#">order now</a>
                                            </div>  
                                        </div>  
                                    </div>              
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slider Caption 2 -->
            <div id="htmlcaption2" class="nivo-html-caption slider-caption-2">
                <div class="slider-text-table">
                    <div class="slider-text-tablecell">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-2 col-sm-3 hidden-xs">
                                    <div class="social-media-follow">
                                        <div class="social-box-inner">
                                            <ul>
                                                <li><a href="https://www.facebook.com/kaayalkerala/" target= "_blank"><i class="mdi mdi-facebook"></i></a></li>
                                                
                                            </ul>   
                                        </div>
                                        <p>follow on</p>
                                    </div>
                                </div>
                                <div class="col-md-10 col-sm-9 col-xs-12">
                                    <div class="slide2-text">
                                        <div class="middle-text">
                                            <div class="title-1 wow rotateInDownRight" data-wow-duration="0.9s" data-wow-delay="0s">
                                                <h2>Welcome to Kaayal Kerala Cuisine </h2>
                                            </div>  
                                            <div class="title-2 wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
                                                <h1></h1>
                                            </div>  
                                            <div class="desc wow slideInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
                                                <p>Please be welcome to share our new dining experience with our mouth watering dishes.
                                                    <br> Enjoy a pleasant dining experience with us or in the comfort of home.
                                                </p>
                                            </div>
                                            <div class="order-now wow bounceInUp" data-wow-duration="1.3s" data-wow-delay=".5s">
                                                <a href="#">order now</a>
                                            </div>  
                                        </div>  
                                    </div>              
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slider Caption 3 -->
             <div id="htmlcaption3" class="nivo-html-caption slider-caption-3">
                <div class="slider-text-table">
                    <div class="slider-text-tablecell">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-2 col-sm-3 hidden-xs">
                                    <div class="social-media-follow">
                                        <div class="social-box-inner">
                                            <ul>
                                                <li><a href="https://www.facebook.com/kaayalkerala/" target= "_blank"><i class="mdi mdi-facebook"></i></a></li>
                                            </ul>   
                                        </div>
                                        <p>follow on</p>
                                    </div>
                                </div>
                                <div class="col-md-10 col-sm-9 col-xs-12">
                                    <div class="slide3-text">
                                        <div class="middle-text">
                                            <div class="title-1 wow rotateInDownRight" data-wow-duration="0.9s" data-wow-delay="0s">
                                                <h2>Welcome to Kaayal Kerala Cuisine </h2>
                                            </div>  
                                            <div class="title-2 wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
                                                <h1></h1>
                                            </div>  
                                            <div class="desc wow slideInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
                                                <p>Please be welcome to share our new dining experience with our mouth watering dishes.
                                                    <br> Enjoy a pleasant dining experience with us or in the comfort of home.
                                                </p>
                                            </div>
                                            <div class="order-now wow bounceInUp" data-wow-duration="1.3s" data-wow-delay=".5s">
                                                <a href="#">order now</a>
                                            </div>  
                                        </div>  
                                    </div>              
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            </div>