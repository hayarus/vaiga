<?php $user = CakeSession::read("Auth.User");?>
<!doctype html>
<html class="" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Kaayal Kerala Cuisine</title>
    <meta name="description" content="Tasty, Quality Kerala Cuisine in Malaysia. Kaayal - the best kerala restuarant in malaysia.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Kerala Restaurant in malaysia, Kerala hotel in malaysia, kerala hotel in pj, kerala hotel in petaling jaya, kerala food in mbpj, Best Malayali food, Best Kerala food, Best kerala Cuisine, Kerala food catering in malaysia, kerala food home delivery in malaysia, malaysia's best kerala food, thani naadan kerala food in malaysia, kerala restuarant in malaysia, kerala restuarants in kl, kerala restuarants in kuala lumpur, adipoli kerala food, kappa meen cury in malaysia, thattu dosa in malaysia, thalsseri biriyani in malaysia, ela sadhya in malaysia, malaysia's favorite kerala restuarant"  charset="utf-8">

        <!--Validation Jquery -->

    <script src="<?php echo $this->webroot; ?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <link href="<?php echo $this->webroot; ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <!--//Validation Jquery -->
   
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $this->webroot; ?>assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script> 
    <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery.bootstrap-growl.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery.bootstrap-growl.min.js"></script>

    <link href="<?php echo $this->webroot; ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->webroot; ?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->webroot; ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo $this->webroot; ?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->webroot; ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->webroot; ?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>
    
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>assets/plugins/select2/select2_metro.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>assets/plugins/data-tables/DT_bootstrap.css"/>
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>assets/plugins/fancybox/source/jquery.fancybox.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>assets/plugins/jquery-multi-select/css/multi-select.css"/>
    <!-- END PAGE LEVEL SCRIPTS -->


    <link href="<?php echo $this->webroot; ?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->webroot; ?>assets/css/pages/news.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->webroot; ?>assets/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>

    <!-- Place favicon.ico in the root directory -->
    <!-- <link href="<?php echo $this->webroot;?>assets/images/apple-touch-icon.png" type="images/x-icon" rel="shortcut icon"> -->
    <!-- All css files are included here. -->
    <link rel="stylesheet" href="<?php echo $this->webroot;?>assets/css/core.css">
    <link rel="stylesheet" href="<?php echo $this->webroot;?>assets/style.css">
    <link rel="stylesheet" href="<?php echo $this->webroot;?>assets/css/responsive.css">
    <!-- customizer style css -->
    <link href="#" data-style="styles" rel="stylesheet">
    <!-- Modernizr JS -->
    <script src="<?php echo $this->webroot;?>assets/js/vendor/modernizr-2.8.3.min.js"></script>

    
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>assets/plugins/lobibox/demo/demo.css"/>
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>assets/plugins/lobibox/dist/css/lobibox.min.css"/>
    <script src="<?php echo $this->webroot; ?>assets/plugins/lobibox/js/lobibox.js"></script>
    <script src="<?php echo $this->webroot; ?>assets/plugins/lobibox/demo/demo.js"></script>

    <!-- <link href="<?php echo $this->webroot; ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->webroot; ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/> -->
<script type="text/javascript">
    $(document).ready(function() {
         setTimeout(function() {
            $("#myModal").modal();
            },3000);

var error1 = $('.alert-danger');
jQuery.validator.addMethod('phoneUS', function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, ''); 
    return this.optional(element) || phone_number.length > 9 &&
        phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
        
}, 'Please enter a valid phone number.');
$('#offercoupon').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message class
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"name" : {required : true },
"emailid" : {required: true,email: true},
"phoneno" : {required : true,
           phoneUS: true,
            },
},
messages:{
"name" : {required :"Please enter Name."},
"emailid" : {required : "Please enter a valid email address",
                  minlength : "Please enter a valid email address"},
"phoneno" : {required :"Please enter phone no."},
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-valid').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-valid').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-valid').removeClass('has-error'); // set success class to the control group
label
.closest('.form-valid').removeClass('error');
},
submitHandler: function (form) {
      var formData = $("#offercoupon").serialize();
      $.ajax({
        type:'POST', 
        data: formData, 
        url: '<?= Router::Url(['controller' => 'pages', 'action' => 'offercoupons']); ?>', 
        success: function(data) {
            if(data){
                $( ".modal-backdrop" ).remove(); 
                $( "#myModal" ).remove(); 
                Lobibox.alert('info', {
                    msg:"Congratulations! You're awarded with 10% off on all dishes. Check your email to get the coupon code."
                }); 
            } 
        }, 
    });
},
});
});

</script>
<script>
    $(document).ready(function(){
    
        $('#emailid').keyup(function(){
            var email = $(this).val();
            $.ajax({
                method:'POST', 
                url:'<?php echo Router::url(['controller' => 'Pages', 'action' => 'getemail']); ?>', 
                dataType: "text", 
                data:{email:email}, 
                success: function(data) 
                {
                    if(data){
                        $( ".result" ).html('<span id="emailspan" style="color:red">Someone already taken this email ID. want to try another?</span>'); 
                    }else{
                        $( ".result" ).html(''); 
                    } 
                } 
            });
        });
    });
</script>
</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  
    
    
    <!-- Pre Loader
    ============================================ -->
    <div class="preloader">
        <div class="loading-center">
            <div class="loading-center-absolute">
                <div class="object object_one"></div>
                <div class="object object_two"></div>
                <div class="object object_three"></div>
            </div>
        </div>
    </div>
  <!-- Body main wrapper start -->
    <div class="wrapper white-bg">
        <!--Header section start-->
        <div class="header sticky-header">
            <?php
            echo $this->element('main-menu');
            ?>
        </div>

       <!--Header section end-->
       
       <!--slider section start-->
       
        <!--slider section end-->
        <?php echo $this->Session->flash(); ?>
        <?php
        echo $this->fetch('content');
        ?>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="btn-color close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><i class="em em-coffee"></i> Get upto 50% off on all dishes by filling this form. <i class="em em-coffee"></i></h4>
                  </div>
                  <div class="modal-body">
                      <form name="offercoupon" id="offercoupon"> 
                      
                        <div class="reserve-form-top"> 
                            <div class="form-valid"> <input type="text" placeholder="Full name" name="name"id="name" > </div>

                            <div class="form-valid"> <input type="text" placeholder="Email address" name="emailid" id="emailid">
                            <div class="result"></div> </div>
                            
                            <div class="form-valid"> <input type="text" placeholder="Mobile number eg:- +00 000000000" name="phoneno" id="phoneno" > </div>

                            <div class="reserve-submit mt-10">
                                <button type="submit">
                                    submit
                                </button>
                            </div>
                            <div class="social-media-icon">
                               <ul> 
                                    <li><a href="https://www.facebook.com/kaayalkerala/" target= "_blank"><i class="mdi mdi-facebook"></i> Like us on facebook</a></li>
                               </ul>
                            </div>
                        </div>
                    </form> 
                </div>
                <div class="modal-footer">
                    <strong>Note:</strong><p> Offer coupon code will send to your email after submitting the form. Present coupon code at bill counter to get offer.</p>
                </div>
              </div>
            </div>
         </div>
        <!--Footer section start-->
        <div class="footer">
            <div class="footer-top ptb-100 grey-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-footer">
                                <h3 class="single-footer-title">Contact us</h3>
                                <div class="single-footer-details mt-30">
                                    <p class="addresses">
                                        <strong>Address: </strong>Kaayal Kerala Cuisine, No 62-G marble jade mansion Jalan behala, <br>Brickfields, Kuala lumpur, Malaysia
                                    </p>

 
                                    <!-- <p class="email">
                                       <strong> Email:</strong> Username@gmail.com<br>Damo@gmail.com
                                    </p> -->
                                    <p class="phon">
                                        <strong>Phone:</strong> 017 879 3996
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-footer">
                                <h3 class="single-footer-title">open hours</h3>
                                <div class="single-footer-details mt-30">
                                    <!-- <p>Lorem ipsum dolor sit amet,  tore latthi dimu consectetueiusmodm dost   </p> -->
                                    <div class="open-list">
                                        <ul>
                                            <li>Monday- Friday  . . . . . . . . . . . . . . . . . . . . . <span>11AM - 10 PM</span></li>
                                            <li>Saturday- Sunday. . . . . . . . . . . . . . . . . . . .<span>11 AM - 11 PM</span></li>
                                            <!-- <li> . . . . . . . . . . . . . . . . . . . . . . . . . . . <span>Close</span></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="single-footer instagram">
                                <h3 class="single-footer-title">instagram</h3>
                                <div class="single-footer-details mt-30">
                                   <ul>
                                       <li><a href="#"><img src="<?php echo $this->webroot;?>assets/images/instagram/1.jpg" alt=""></a></li>
                                       <li><a href="#"><img src="<?php echo $this->webroot;?>assets/images/instagram/2.jpg" alt=""></a></li>
                                       <li><a href="#"><img src="<?php echo $this->webroot;?>assets/images/instagram/3.jpg" alt=""></a></li>
                                       <li><a href="#"><img src="<?php echo $this->webroot;?>assets/images/instagram/4.jpg" alt=""></a></li>
                                       <li><a href="#"><img src="<?php echo $this->webroot;?>assets/images/instagram/5.jpg" alt=""></a></li>
                                       <li><a href="#"><img src="<?php echo $this->webroot;?>assets/images/instagram/6.jpg" alt=""></a></li>
                                   </ul>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-footer newsletter">
                                <h3 class="single-footer-title">Newsletter</h3>
                                <div class="single-footer-details mt-30">
                                    <!-- <p>Lorem ipsum dolor sit amet,  tore latthi dimu consectetueiusmodm dost </p> -->
                                    <div class="newsletter-form">
                                        <form action="#">
                                            <input type="text" placeholder="Enter your email">
                                            <button type="submit">submit</button>
                                        </form>   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
            <div class="popup"></div>
            <div class="copyright text-center ptb-20 white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <p>Copyright&copy;<?php echo date("Y");?>.All right reserved.Created by <a target="_blank" href="http://hayarus.com">Hayarus Solutions</a></p>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
        <!--Footer section end-->
    </div>
    <!-- Body main wrapper end -->
    
   
    <!-- Placed js at the end of the document so the pages load faster -->

    
    <!-- All js plugins included in this file. -->
   

    <!-- <script src="<?php echo $this->webroot;?>assets/js/vendor/jquery-1.12.0.min.js"></script> -->
    <script src="<?php echo $this->webroot;?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/jquery.nivo.slider.pack.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/ajax-mail.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/jquery.magnific-popup.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/animated-headlines.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/waypoints.min.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/jquery.collapse.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/plugins.js"></script>
    <script src="<?php echo $this->webroot;?>assets/js/main.js"></script>

      

      <?php
    if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();?>
<script src="<?php echo $this->webroot; ?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo $this->webroot; ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/data-tables/DT_bootstrap.js"></script>
<?php /*?><script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/flowplayer/flowplayer.min.js"></script><?php */?>
<!-- END PAGE LEVEL PLUGINS -->
<!--<script src="<?php echo $this->webroot;?>js/colorbox/jquery.colorbox.js"></script>-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php /*?><script src="<?php echo $this->webroot; ?>assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script><?php */?>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery.pulsate.min.js" ></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
<?php /*?><script src="<?php echo $this->webroot; ?>assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script><?php */?>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>

<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/clockface/js/clockface.js"></script>
<!-- <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<!-- <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> -->



<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<?php /*?><script src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><?php */?>
<script src="<?php echo $this->webroot; ?>assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<?php /*?><script src="<?php echo $this->webroot; ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script><?php */?>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $this->webroot; ?>assets/scripts/app.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/jquery.validate.custom.rules.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/index.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/tasks.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<?php /*?><script src="<?php echo $this->webroot; ?>assets/scripts/form-components.js"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/form-validation.js"></scrip

<?php */?>
<?php /*?><script src="<?php echo $this->webroot; ?>assets/scripts/table-managed.js"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/table-advanced.js"></script><?php */?>

<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<script src="<?php echo $this->webroot; ?>assets/scripts/portfolio.js"></script>

<script src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo $this->webroot; ?>assets/plugins/dropzone/dropzone.js"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/app.js"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/form-components.js"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/ui-extended-modals.js"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo $this->webroot; ?>assets/scripts/form-dropzone.js"></script>
    <?php //}?>

<script>
jQuery(document).ready(function() { 

   App.init(); // initlayout and core plugins
   Index.init();
   FormComponents.init();
   FormDropzone.init();
   //Portfolio.init();

   //accreditation status for users
   $('#accreditation').change(function() {
        $("#accreditation").submit();
   });

        
});


</script>
<?php echo $this->Js->writeBuffer(); ?>
<!-- END JAVASCRIPTS -->  
</body>

</html>
<style type="text/css">
    .form-valid input {
    border: 2px solid #eceff8;
    height: 45px;
    box-shadow: none;
    padding-left: 10px;
    font-size: 14px;
    width: 100%;
    }
    .form-valid> input {
     border: 1px solid #ddd;
     margin-bottom: 18px;
     text-transform: capitalize;
     color: #aaa
    }
    .form-valid button {
    border: 2px solid #eceff8;
    height: 45px;
    position: fixed;
    }
    .input-group-addon, .input-group-btn {
    width: 1%;
    white-space: nowrap;
     vertical-align: top!important;
    }

    .modal-title{
    text-align: center;
    }

    .modal-footer{
    text-align: left!important;
    }

    .modal-footer p{
    font-style: italic
    }

    .social-media-icon{
    margin-top:25px;
    text-align: center;
    }
    .social-media-icon ul {
    text-align: center;
    }

    .social-media-icon a{
    padding: 10px;
    background-color: #4267b2;
    color:#fff!important; 
    }
    .result{
    top:-30px;
    margin-bottom: 5px;
    }
    .help-block {
    margin-top: -10px!important;  

    }
</style>