
<?php $this->Html->addCrumb('Menuitem', '/admin/menuitems'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<script type="text/javascript">

$(document).ready(function(){
var error1 = $('.alert-danger');
$('#MenuitemAdminAddForm').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message classfalse
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"data[Menuitem][name]" : {required : true },
"data[Menuitem][imgname]" : {required : true, accept: "jpg|jpeg|png|ico|bmp" },
"data[Menuitem][category]" : {required : true,value:true },
"data[Menuitem][details]" : {required : true },
"data[Menuitem][price]" : {required : true,number: true }
},
messages:{
"data[Menuitem][name]" : {required :"Please enter name."},
"data[Menuitem][imgname]" : {required :"Please select image.",accept: "Choose a valid file format."},
"data[Menuitem][category]" : {required :"Please select category."},
"data[Menuitem][details]" : {required :"Please enter details."},
"data[Menuitem][price]" : {required : "Please enter price." }
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-group').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-group').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-group').removeClass('has-error'); // set success class to the control group
label
.closest('.form-group').removeClass('error');
},
});
});

</script>

<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i><?php echo __('Add Menuitem'); ?>                        
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body form users">
						<div class="alert alert-danger display-hide">
						<button data-close="alert" class="close"></button>
						You have some form errors. Please check below.
						</div>

						<?php echo $this->Form->create('Menuitem', array('class' => 'form-horizontal','enctype'=>'multipart/form-data','method'=>'post')); ?>

						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">&nbsp;</label>
								<div class="col-md-4">
									<div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
								</div>
							</div>   
							
							<div class="form-group">
								<?php echo $this->Form->label('image<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('imgname', array('class' => 'form-control', 'type'=>'file', 'label' => false, 'required' => false));?>
								</div>
							</div>   
							<div class="form-group"> 
							<?php echo $this->Form->label('Name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('detail<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('details', array('class' => 'form-control', 'type'=>'textarea', 'label' => false, 'required' => false));?>
								</div>
							</div>  
							<div class="form-group">
							<?php echo $this->Form->label('Category<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
								<?php echo $this->Form->input('menucategory_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('price<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('price', array('class' => 'form-control', 'type'=>'text', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<!-- <div class="form-group">
								<?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false, 'options' => $boolenStatus));?>
								</div>
							</div> -->
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/menuitems'; ?>'">Cancel</button>
								</div>
							</div>
						</div>
						
						<?php echo $this->Form->end(); ?>
					<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
