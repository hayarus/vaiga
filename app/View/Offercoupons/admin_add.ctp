
<?php $this->Html->addCrumb('Offer coupon', '/admin/offercoupons'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#OffercouponAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Offercoupon][coupon]" : {required : true},
"data[Offercoupon][name]" : {required : true},
"data[Offercoupon][email]" : {required : true , email: true},
"data[Offercoupon][phone]" : {required : true,
							  number : true,
					          minlength:10,
					          maxlength:10
					          },
		},
		messages:{
		"data[Offercoupon][coupon]" : {required :"Please enter coupon."},
"data[Offercoupon][name]" : {required :"Please enter name."},
"data[Offercoupon][email]" : {required :"Please enter an email id."},
"data[Offercoupon][phone]" : {required :"Please enter phone."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add Offer Coupon'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form offercoupons">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Offercoupon', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
<div class="form-group">
							 <?php echo $this->Form->label('coupon<span class=required>  </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('coupon', array('class' => 'form-control', 'label' => false, 'value'=>$coupon , 'required' => false , 'readonly'));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('email<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								<div class="result"></div>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('phone<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('phone', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/offercoupons'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	 $(document).ready(function(){
    
        $('#OffercouponEmail').keyup(function(){
            var email = $(this).val();
            $.ajax({
                method:'POST', 
                url:'<?php echo Router::url(['controller' => 'Offercoupons', 'action' => 'getemail']); ?>', 
                dataType: "text", 
                data:{email:email}, 
                success: function(data) 
                {
                    if(data){
                        $(".result").html('<span style="color:red">Someone Already Has That Email Id. Try another?</span>'); 
                    }else{
                        $(".result").html(''); 
                    } 
                } 
            });
        });
    });
</script>