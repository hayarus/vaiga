<!--Breadcrubs start-->
        <div class="breadcrubs">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcurbs-inner text-center">
                            <h3 class="breadcrubs-title">
                                Our blog
                            </h3>
                            <ul>
                                <li><a href="<?php echo $this->webroot;?>">home <span>//</span>  </a></li>
                                <li>Our blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--Breadcrubs end-->
       <!--our blog section start-->
        <div class="our-blog-pages ptb-80">
            <div class="bg-mg-1">
                <div class="container">
                    <div class="row">
                        <!-- <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog mb-30">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/1.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>13<span>Mar</span></p>
                                    </div>
                                    <div class="blog-title">
                                        <h3><a href="#">Lorem Ipsum is simply dummy</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog blog-video mb-30">
                                <div class="blog-img">
                                    <div class="blog-thumbnail">
                                        <img src="<?php echo $this->webroot;?>assets/images/blog/2.jpg" alt="">
                                    </div>
                                    <div class="blog-hover">
                                        <a href="https://www.youtube.com/embed/oxPgDogVFnQ"><i class="mdi mdi-play"></i></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog mb-30">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/3.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>13<span>Mar</span></p>
                                    </div>
                                    <div class="blog-title">
                                        <h3><a href="#">Lorem Ipsum is simply dummy</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog mb-30">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/seafood.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>21<span>Oct</span></p>
                                    </div>
                                    <div class="blog-title">
                                        <h3><a href="<?php echo $this->webroot;?>/Pages/blog_details">Sea Food Fest</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog mb-30">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/seafood2.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>21<span>Oct</span></p>
                                    </div>
                                    <div class="blog-title">
                                         <h3><a href="<?php echo $this->webroot;?>/Pages/blog_details">Sea Food Fest</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog mb-30">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/seafood3.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>21<span>Oct</span></p>
                                    </div>
                                    <div class="blog-title">
                                        <h3><a href="<?php echo $this->webroot;?>/Pages/blog_details">Sea Food Fest</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/7.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>13<span>Mar</span></p>
                                    </div>
                                    <div class="blog-title">
                                        <h3><a href="#">Lorem Ipsum is simply dummy</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog blog-video">
                                <div class="blog-img">
                                    <div class="blog-thumbnail">
                                        <img src="<?php echo $this->webroot;?>assets/images/blog/8.jpg" alt="">
                                    </div>
                                    <div class="blog-hover">
                                        <a href="https://www.youtube.com/embed/oxPgDogVFnQ"><i class="mdi mdi-play"></i></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 hidden-sm col-xs-12">
                            <div class="single-blog">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/9.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>13<span>Mar</span></p>
                                    </div>
                                    <div class="blog-title">
                                        <h3><a href="#">Lorem Ipsum is simply dummy</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!--our blog section end-->
    