<!-- Breadcrubs start-->
        <div class="breadcrubs">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcurbs-inner text-center">
                            <h3 class="breadcrubs-title">
                                Blog details
                            </h3>
                            <ul>
                                <li><a href="<?php echo $this->webroot;?>">home <span>//</span>  </a></li>
                                <li>Blog details</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <!--Breadcrubs end-->
  <!--Blog details start -->
        <div class="blog-details-page ptb-80">
            <div class="bg-img"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="blog-left-sidebar">
                            <article class="articles-details">
                                <div class="blog-thumbnail mb-60">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/blog-details.jpg" alt="">
                                    <div class="blog-publish">
                                        <div class="publish-date">
                                            <p>21<span>Oct</span></p>
                                        </div>
                                        <div class="blog-title">
                                            <h4> Vaiga Food Fest </h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="blog-desacription fix">
                                    <div class="b-desc-1">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut rutrum nunc. Donec rhoncus lacus sed mauris feugiat ultrices. Mauris ish veles  ish sapien sem. lovess uisque nec lectus sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut fermentum est, it’ss laoreet is congue nulla. Mauris bibendumess pellentesque facilisis. Maecenas tw ante odio, rutrum nec viverra.justo ma tristique mi, lorem ipsum dolor sit amet. rutrum nec viverra.justo magna tristique mi, aenean nis massa,scelerisque impertied feubiat.</p>
                                    </div>
                                    <div class="b-desc-2">
                                        <p>Aenean nisl massa, scelerisque imperdiet feugiat et, rutrum ut nibh. Integer elit sem, rutrum vestibulum nunc nec, imperdiet tincidunt es odio. Aenean ant aliquet, ante non pellentesque laoreet, lorem leo egestas metus, eget pellentesque nisi sem non ex. </p>
                                    </div>
                                </div>
                                <div class="blog-action">
                                    <!-- <div class="action-box">
                                        <p><i class="mdi mdi-heart-outline"></i>(500)</p>
                                        <p><i class="mdi mdi-comment-multiple-outline"></i>(100)</p>
                                        <p><i class="mdi mdi-share-variant"></i>(50)</p>
                                    </div>
                                    <div class="blog-share-social">
                                        <a href="#"><i class="mdi mdi-facebook"></i></a>
                                        <a href="#"><i class="mdi mdi-twitter"></i></a>
                                        <a href="#"><i class="mdi mdi-dribbble"></i></a>
                                        <a href="#"><i class="mdi mdi-pinterest"></i></a>
                                    </div> -->
                                </div>
                                <!-- <div class="blog-cheaf-quote">
                                    <div class="cheaf-quote-img">
                                        <img src="<?php echo $this->webroot;?>assets/images/blog/cheap-quote.jpg" alt="">
                                    </div>
                                    <div class="cheaf-quote-desc fix">
                                        <div class="cheaf-quote-text">
                                            <p><i class="fa fa-quote-left"></i>Aenean nisl massa, scelerisque imperdiet feugiat et, rutrum ut nibh. elitlove sem, Aenean nisl massa, scelerisque.<i class="fa fa-quote-right"></i></p> 
                                            <div class="cheaf-quote-action">
                                                <div class="cheaf-quote-title">
                                                    <h5>Sumayia Akter</h5>
                                                    <p>Manager</p>    
                                                </div> 
                                                <div class="cheaf-quote-social">
                                                    <a href="#"><i class="mdi mdi-facebook"></i></a>
                                                    <a href="#"><i class="mdi mdi-twitter"></i></a>
                                                    <a href="#"><i class="mdi mdi-dribbble"></i></a>
                                                    <a href="#"><i class="mdi mdi-pinterest"></i></a>
                                                </div>   
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="blog-comment-box">
                                    <div class="comment-title">
                                       <!-  <h3>Awesome commnents</h3> 
                                    </div>
                                    <div class="comment-box-inner fix">
                                        <div class="blog-comment fix">
                                            <div class="blog-comment-img">
                                                <img src="<?php echo $this->webroot;?>assets/images/blog/blog-comment-1.jpg" alt="">
                                            </div> 
                                            <div class="blog-comment-desc">
                                                <div class="comment-top-box">
                                                    <div class="comment-title-box">
                                                        <h5>Tarek Aziz</h5>
                                                        <p>15 Min ago</p>
                                                    </div>  <div class="comment-action-box">
                                                        <a href="#"><i class="mdi mdi-heart-outline"></i>(50)</a>
                                                        <a href="#"><i class="mdi mdi-share"></i>(20)</a>
                                                    </div> 
                                                </div>
                                                <div class="comment-bottom-box">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquam, quam congue dictum luctus, lacus magna congue ante, in finibus dui sapien eu dolor. Integer tincidunt suscipit erat, nec laoreet ipsum vestibulum sed. </p>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="blog-comment reply fix">
                                             <div class="blog-comment-img">
                                                <img src="<?php echo $this->webroot;?>assets/images/blog/blog-comment-2.jpg" alt="">
                                            </div>
                                            <div class="blog-comment-desc">
                                                <div class="comment-top-box">
                                                    <div class="comment-title-box">
                                                        <h5>Mohin patwary</h5>
                                                        <p>14 Min ago</p>
                                                    </div>
                                                     <div class="comment-action-box">
                                                        <a href="#"><i class="mdi mdi-heart-outline"></i>(50)</a>
                                                        <a href="#"><i class="mdi mdi-share"></i>(20)</a>
                                                    </div>
                                                </div>
                                                <div class="comment-bottom-box">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquam, quam congue dictum luctus, lacus magna congue ante, in finibus dui sapien eu dolor. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    blog comment form start-->
                                    <!-- <div class="blog-comment-form mt-40">
                                        <div class="comment-title">
                                            <h3>leave a comment</h3>
                                        </div>
                                        <form action="#">
                                            <div class="comment-input fix">
                                                <div class="input-field">
                                                    <input type="text" placeholder="Name">
                                                </div>
                                                <div class="input-field">
                                                    <input type="text" placeholder="E-mail">
                                                </div>
                                            </div>
                                            <div class="textarea">
                                                <textarea placeholder="Message"></textarea>
                                            </div>
                                            <div class="submit">
                                                <button type="submit">Send Message</button>
                                            </div>
                                        </form>
                                    </div> 
                                    
                                </div> -->
                                <!--blog comment form end-->
                            </article> 
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="blog-right-sidebar">
                            <aside class="widget search mb-30">
                                <div class="widget-search">
                                    <form action="#">
                                        <input type="text" placeholder="Search here....">
                                        <button type="submit"><i class="mdi mdi-magnify"></i></button>
                                    </form>
                                </div>    
                            </aside>
                            <aside class="widget mb-30">
                                <div class="widget-title">
                                    <h3>categories</h3>
                                </div>
                                <div class="widget-categories"> 
                                    <!--Accordion item 1--> 
                                    <h6>breakfase</h6>
                                    <ul>
                                        <li><a href="#">Bread</a></li>
                                        <li><a href="#">Coffie</a></li>
                                    </ul>
                                    <!--Accordion item 1 end--> 
                                    
                                    <!--Accordion item 2 start--> 
                                    <h6>lunch</h6>
                                    <ul>
                                        <li><a href="#">Rice</a></li>
                                        <li><a href="#">Meat</a></li>
                                    </ul>
                                    <!--Accordion item 2 end--> 
                                    
                                    <!--Accordion item 3 start--> 
                                    <h6>dinner</h6>
                                    <ul>
                                        <li><a href="#">Fizza</a></li>
                                        <li><a href="#">Chiken soap</a></li>
                                    </ul>
                                    <!--Accordion item 3 end--> 
                                    
                                    <!--Accordion item 4 start--> 
                                    <h6 class="open">Dessert</h6>
                                        <ul>
                                            <li><a href="#">Ice-cream</a></li>
                                    </ul>
                                    <!--Accordion item 4 end--> 
                                    
                                    <!--Accordion item 5 start--> 
                                    <h6>Drinks</h6>
                                    <ul>
                                        <li><a href="#">Cook</a></li>
                                        <li><a href="#">Tea</a></li>
                                    </ul>
                                    <!--Accordion item 5 end--> 
                                </div>
                            </aside>
                            <aside class="widget mb-30">
                                <div class="widget-title">
                                    <h3>recent post</h3>
                                </div>
                                <div class="recent-post">
                                    <div class="single-recent-post mb-15">
                                        <div class="recent-post-thumbnail">
                                            <img src="<?php echo $this->webroot;?>assets/images/blog/recent-1.jpg" alt="">
                                        </div>
                                        <div class="post-detail">
                                            <div class="post-title">
                                                <h5><a href="#">Vaiga Food Fest</a></h5>
                                            </div>
                                            <div class="post-publish">
                                                <p class="post-date">
                                                    On 21 Oct 2018 
                                                </p>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="single-recent-post mb-15">
                                        <div class="recent-post-thumbnail">
                                            <img src="<?php echo $this->webroot;?>assets/images/blog/recent-2.jpg" alt="">
                                        </div>
                                        <div class="post-detail">
                                            <div class="post-title">
                                                <h5><a href="#">Vaiga Food Fest</a></h5>
                                            </div>
                                            <div class="post-publish">
                                                <p class="post-date">
                                                    On 21 Oct 2018 
                                                </p>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="single-recent-post">
                                        <div class="recent-post-thumbnail">
                                            <img src="<?php echo $this->webroot;?>assets/images/blog/recent-3.jpg" alt="">
                                        </div>
                                        <div class="post-detail">
                                            <div class="post-title">
                                                <h5><a href="#">Vaiga Food Fest</a></h5>
                                            </div>
                                            <div class="post-publish">
                                                <p class="post-date">
                                                    On 21 Oct 2018 
                                                </p>
                                            </div>
                                        </div>
                                    </div>    
                                </div>    
                            </aside>
                            <!-- <aside class="widget video mb-30">
                                <div class="widget-video">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/sidebar-video.jpg" alt="">
                                    <div class="widget-video-overlay">
                                        <a href="https://www.youtube.com/watch?v=oxPgDogVFnQ">
                                            <i class="mdi mdi-play"></i>
                                        </a>
                                    </div>
                                </div>
                            </aside> -->
                           <!--  <aside class="widget tag">
                                <div class="widget-title">
                                    <h3>tag</h3>
                                </div>
                                <div class="widget-tag">
                                    <ul>
                                        <li><a href="#">Breakfast&lunch</a></li>
                                        <li><a href="#">events</a></li>
                                        <li><a href="#">drinks</a></li>
                                        <li><a href="#">dessert</a></li>
                                        <li><a href="#">dinner&event</a></li>
                                    </ul>
                                </div>
                            </aside> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Blog details end-->
        <style type="text/css">
            .widget ,.mb-30{
                background-color: #fff!important;
            }
        </style>