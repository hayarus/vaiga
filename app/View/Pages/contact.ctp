
 <!--Breadcrubs start-->
        <div class="breadcrubs">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcurbs-inner text-center">
                            <h3 class="breadcrubs-title">
                               Contact Us
                            </h3>
                            <ul>
                                <li><a href="<?php echo $this->webroot;?>">home <span>//</span>  </a></li>
                                <li>Contact us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--Breadcrubs end-->
       <!--contact us pages start-->
        <div class="contact-us">
            <div class="contact-information text-center ptb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="single-contact-information">
                                <div class="contact-icon">
                                    <a href="#"><i class="mdi mdi-phone"></i></a>
                                </div>
                                <p>017 879 3996</p>
                                <!-- <p>+012 345 678 102</p> -->
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="single-contact-information">
                                <div class="contact-icon">
                                    <a href="#"><i class="mdi mdi-dribbble"></i></a>
                                </div>
                                <p>info@kaayalkeralacuisine.com</p>
                                <p>www.Kaayalkeralacuisine.com</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="single-contact-information">
                                <div class="contact-icon">
                                    <a href="#"><i class="mdi mdi-map-marker"></i></a>
                                </div>
                                <!-- <p>Address goes here,</p>
                                <p>street,Crossroad123.</p> -->
                                <p>Kaayal Kerala Cuisine, No 62-G marble jade mansion Jalan behala,<br/> Brickfields, Kuala Lumpur,<br/>  Malaysia</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <!--Contact bottom section-->

<script type="text/javascript">
$(document).ready(function(){
var error1 = $('.alert-danger');
$('#contact').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message class
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"name" : {required : true },
"emailid" : {required: true,email: true},
"message" : {required : true,minlength:10 },
// "data[Image][name]" : {required : true, accept: "jpg|jpeg|png|ico|bmp" },
// "data[Image][status]" : {required : true }
},
messages:{
"name" : {required :"Please enter name."},
"emailid" : {required : "Please enter a valid email address",
             minlength : "Please enter a valid email address"},
"message" : {required :"Please enter message."},

// "data[Image][name]" : {required :"Please select image.",accept: "Choose a valid file format."},
// "data[Image][status]" : {required :"Please enter status."}
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-valid').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-valid').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-valid').removeClass('has-error'); // set success class to the control group
label
.closest('.form-valid').removeClass('error');
},
});
});
</script>
            <div class="contact-bottom-section ptb-100">
                <div class="bg-img"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 contact-form-div">
                            <div class="contact-form">
                                <div class="contact-form-title">
                                    <h2>Get In Touch</h2>
                                </div>
                                <div class="contact-form-box">
                                    <p class="form-messege"></p>
                                    <form name="contact" id="contact"  method="post" >
                                      <!--   <input name="name" type="text" placeholder="Name">
                                        <input name="email" type="text" placeholder="Email">
                                        <textarea name="message" placeholder="Message"></textarea>
                                        <button type="submit">Submit</button>
                                    </form>
                                      -->
                                        <div class="form-valid"><input type="text" name="name" id="name" placeholder="Name"></div> 
                                        <div class="form-valid"><input type="email" name="emailid" id="email" placeholder="Email Address"></div>                            
                                        <!-- <input type="text" class="form-control"  name="data[subject]" id="data[subject]" placeholder="Subject"> --> 
                                        <div class="form-valid"><textarea name="message" id="message" placeholder="Message"></textarea></div>
                                        <button type="submit">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 map-div">
                            <div id="contact-map" class="map-area">
                                <div id="googleMap" style="width:100%;height:480px;">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d497.9837954478737!2d101.68881578245514!3d3.1289522784356674!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4ba67b183463%3A0x56ef5f5bf41917d9!2sKaayal+Kerala+Cuisine!5e0!3m2!1sen!2smy!4v1558525661091!5m2!1sen!2smy" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <!--Contact bottom section end-->
            
        </div>
       <!--contact us pages end-->