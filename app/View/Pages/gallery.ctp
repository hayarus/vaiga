<!--Breadcrubs start-->
        <div class="breadcrubs ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcurbs-inner text-center">
                            <h3 class="breadcrubs-title">
                               Our gallery
                            </h3>
                            <ul>
                                <li><a href="<?php echo $this->webroot;?>">home <span>//</span>  </a></li>
                                <li>our gallery</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrubs end-->
        <!--Our gallery start-->
        <div class="our_gallery ptb-80">
            <div class="bg-img-1">
                <div class="container">
                    <div class="row">
                    <?php  foreach ($images as $key => $image) { ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-gallery-img mb-30">
                            <a href="<?php echo $this->webroot.$ImagelargePath.$image['Gallery']['imgname'];?>"alt="">
                            <img src="<?php echo $this->webroot.$ImagesmallPath.$image['Gallery']['imgname'];?>" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div>
                    <?php }?>
                        <!-- <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-gallery-img mb-30">
                                <a href="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-2.jpg">
                                    <img src="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-2.jpg" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-gallery-img mb-30">
                                <a href="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-3.jpg">
                                    <img src="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-3.jpg" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-gallery-img mb-30">
                                <a href="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-4.jpg">
                                    <img src="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-4.jpg" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-gallery-img mb-30">
                                <a href="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-5.jpg">
                                    <img src="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-5.jpg" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-gallery-img mb-30">
                                <a href="images/gallery/gallery-2/g-6.jpg">    
                                    <img src="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-6.jpg" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-gallery-img">
                                <a href="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-7.jpg">
                                    <img src="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-7.jpg" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-gallery-img">
                                <a href="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-8.jpg">
                                    <img src="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-8.jpg" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div>
                        <div class="col-md-4 hidden-sm col-xs-12">
                            <div class="single-gallery-img">
                               <a href="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-9.jpg">
                                    <img src="<?php echo $this->webroot;?>assets/images/gallery/gallery-2/g-9.jpg" alt="">
                                    <div class="single-gallery-hover">
                                        <span><i class="fa fa-search-plus"></i></span>
                                    </div>
                                </a>
                            </div>   
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!--Our gallery end-->