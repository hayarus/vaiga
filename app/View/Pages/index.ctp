<script src="https://www.google.com/recaptcha/api.js" async defer></script>
 <!--Home about section start-->
            <!-- Slider Image -->
           <?php
            echo $this->element('slider');
            ?>
        
        <div class="home-about white-bg ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="welcome-about">
                            <h2 class="title_1">About Kaayal Kerala Cuisine </h2>
                            <!-- <h3 class="title_2">Vestibulum quis elit eget neque porttitor  no amet dolor. Proin pretiumss.  </h3> -->
                            <p class="text1">Spices have always played an integral part of Kerala's history. Our food is an example of the very same and we have offerings for people from all over the world. Be it famous international restaurant, we have everything a foodie could ask for prepared with that special Kerala taste to it. The cuisine here caters to vegetarians and non-vegetarians alike. Let the flavors of our past and our future take you on a delightful journey. </p>
                            <!-- <p class="text2"> Be it famous international restaurant, we have everything a foodie could ask for prepared with that special Kerala taste to it. The cuisine here caters to vegetarians and non-vegetarians alike. Let the flavors of our past and our future take you on a delightful journey. </p> -->
                            <div class="read-more">
                                <a href="<?php echo $this->webroot;?>pages/aboutus">read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="welcome-about-img">
                            <img src="<?php echo $this->webroot;?>assets/images/about/home-about.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Home about section end-->
         <!--popular dises start-->
        <div class="popular-dishes">
            <div class="bg-img-2 ptb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-title text-center grey_bg">
                                <h2>Top favorites foods</h2>
                                <p style="text-align: justify;">  These Exotic dishes of Kerala are unmatchable with its unique flavours and they are yummy, nutritious and a must try.
                               <!--  The food in Kerala is as awesome as the God’s own country and the people here. You have so much to explore, see, eat and drink if you are much inclined towards the love for travelling and eating.
                                Kerala, also known as the land of spices has more of non-vegetarians than vegetarians and so you can find the hotels and restaurants serve chicken, mutton, beef, pork, and sea food such as mussels, crabs, prawns, squid, oysters, sardines, tuna, lobsters and many more.
                                You will find the aroma of chilly, mustard, curry leaves, coconut oil, and coconut chopped, grated and even coconut milk dominating most of the dishes forcing you to fall in love with the cuisine of Kerala at Vaiga Cafe. -->
                                </p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="dises-list">
                                    <div class="dises-show text-center">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/puttu.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Puttu and Kadala Curry</a></h3>
                                                    <!-- <p class="price">$100</p> -->


                                                    <p style="text-align: justify;">Puttu is one of the very famous healthy breakfast dishes of kerala. The rice flour is steamed in the preparation of puttu. Puttu is often served along with gravies, like fish curry, chicken curry or kadala (chickpea) curry.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/dosa.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Dosa with Sambar and Chutney</a></h3>
                                                    <!-- <p class="price">$100</p> -->
                                                    <p style="text-align: justify;">Dosas prepared from fermented rice and lentils is another favourite breakfast dish enjoyed by all. It is often served with chutneys and sambar. There are varieties of Dosas for example Ghee dosa, Uthappam, Cheese dosas, paneer dosas to name a few.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 hidden-sm col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/iddly.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Idli Sambar/ Chutney</a></h3>
                                                    <!-- <p class="price">$100</p> -->

                                                    <p style="text-align: justify;">Idli prepared from the fermented rice and lentils is usually steamed and has been universally accepted for the health benefits that it has got.It is usually combined with hot steaming sambar and coconut chutney<!-- .Idli Sambar/ Chutney are another breakfast delight of Malayalees. --></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="dises-show text-center">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/appam.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href=""></a>Vellappam and Stew</h3>
                                                    <!-- <p class="price">$100</p> -->
                                                    <p style="text-align: justify;">Vellappam is a soft ,easy fluffy oil free  breakfast delight. It can be  any meal of the day as a snack , lunch or dinner.Appams best goes with mutton curry, fish molly and Vegetable stew.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/idiyappam.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Idiyappam and Egg curry</a></h3>
                                                    <!-- <p class="price">$100</p> -->
                                                    <p style="text-align: justify;">Idiyappam or string hoppers is a popular breakfast dish. A perfect combination would be with Vegetable Kuruma or kerala egg varieties.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 hidden-sm col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/elasadya.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href=""></a>Ela Sadya</h3>
                                                    <p style="text-align: justify;">A royal lunch that combines a variety of dishes starting from pachadi, kichdi, pulliserry, olan, sambar, varavu, thoran, aviyal, sambar, and what not. The banana leaf served with hot steaming rice will lure you,Payasam is served for dessert.
                                                                        </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="dises-show text-center">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/fry.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Spicy Chicken fry </a></h3>
                                                    <p style="text-align: justify;">Chicken fried with onion,spices,garlic,and vinegar on a banana leaf.You can have it with chapattis,Kerala Porotta,Appam or rice.Some of the street hotels or thatukadas in Kerala serve Nadan Kozhi Varthatu with Dosa.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/prawn.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Kerala Style Prawn Curry</a></h3>

                                                    <p style="text-align: justify;">The traditional prawn curry is sprinkled with chilly, pepper, salt and then cooked in whole coconut milk and jiggery and then garnished with curry leaves.Prawns curry is one of the specialties of Kerala food. </p>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 hidden-sm col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/pumpkin.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Pumpkin and Lentil Stew </a></h3>
                                                    <p style="text-align: justify;">If you are a vegetarian and would like to explore the vegetarian cuisines of Kerala in depth, then Erissery should top your list.Prepared from pumpkin and lentils, this stew is sweet to taste .Erissery is usually served with rice.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                     <div class="dises-show text-center">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/fish.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Kerala Style Fish Molee </a></h3>
                                                    <p style="text-align: justify;">It is another exquisiteness from God’s own country.
                                                    Kudampulli and coconut combined with Sea or Fresh water Fish to provide a blend of awesomeness.You can have it with Kerala Porotta,Appam or Rice.
                                                    <span></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/biriyani.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Thalaserry Biryani</a></h3>
                                                   
                                                    <p style="text-align: justify;">Another exotic cuisine of Kerala is the biryani.Especially the biryani from Calicut and Thalaserry are something that your taste buds cannot resist for the awesomeness that they offer.Chillychicken fry tastes great with biryani.
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                      <!-- <div class="dises-show text-center">
                                        
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/2.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Ela Sadya</a></h3>
                                                    <p class="price">$100</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 hidden-sm col-xs-12">
                                            <div class="single-disesh">
                                                <div class="disesh-img">
                                                    <a href="#"><img src="<?php echo $this->webroot;?>assets/images/dish/3.png" alt=""></a>
                                                </div>
                                                <div class="disesh-desc pt-50">
                                                    <h3><a href="">Spicy Chicken fry </a></h3>
                                                    <p class="price">$100</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>   -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--popular dises end-->
         <!--Food menu section start-->
        <!-- uncomment this <div class="food-menu white-bg ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title mb-50 text-center white_bg">
                            <h2 class="mb-50">Our Food Menu</h2>
                            <p></p>
                        </div>
                    </div>
                </div> -->
                <!-- uncomment this <div class="row">
                    <div class="food-item-tab home-page">
                        <div class="col-md-12">
                            <div class="foode-item-box fix mb-60">
                                <ul class="nav" role="tablist">
                                    <li role="presentation" class="active"><a aria-controls="tab1" href="#tab1"  data-toggle="tab">All item</a></li>
                                    <li role="presentation"><a href="#tab2" aria-controls="tab2" data-toggle="tab">breakfast</a></li>
                                    <li role="presentation"><a href="#tab3" aria-controls="tab3" data-toggle="tab">lunch</a></li>
                                    <li role="presentation"><a href="#tab4" aria-controls="tab4" data-toggle="tab">dinner</a></li> -->
                                    <!-- <li role="presentation"><a href="#tab5" aria-controls="tab5" data-toggle="tab">drink</a></li> -->
                                    <!-- <li role="presentation"><a href="#tab6" aria-controls="tab6" data-toggle="tab">party</a></li> -->
                                    <!-- <li role="presentation"><a href="#tab7" aria-controls="tab7" data-toggle="tab">coffee</a></li> -->
                          <!--    uncomment this   </ul>
                            </div>
                        </div>
                        <div class="food-item-desc">
                            <div class="col-md-12">
                                <div class="tab-content"> -->

                                 <!-- My Testing -->
                                  <!-- tab1 -->
                                <!-- uncomment this <div role="tabpanel" class="tab-pane active" id="tab1">
                                        <div class="row">
                                        <?php  foreach ($allmenuitems as $key => $allmenuitem) { ?>
        
    
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$allmenuitem['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $allmenuitem['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $allmenuitem['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $allmenuitem['Menuitem']['price'];?></p> -->
                                                                <!-- <p>$<?php echo  $allmenuitem['Menuitem']['price'];?></p> -->
                                                             <!-- uncomment this   </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            <?php  } ?>          
                                        </div>  
                                     </div>  -->
                                    <!-- // End tab1// -->
                                <!-- tab 2 -->
                                 <!-- uncomment this <div role="tabpanel" class="tab-pane" id="tab2">
                                    <div class="row">
                                        <?php  foreach ($breakfasts as $key => $breakfast) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$breakfast['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $breakfast['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $breakfast['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $breakfast['Menuitem']['price'];?></p> -->
                                                                <!-- &#8377;<p>$<?php echo  $breakfast['Menuitem']['price'];?></p> -->
                                                             <!-- uncomment this   </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div> -->
                                <!-- // End tab 2// -->
                                <!-- tab 3 -->
                                 <!-- uncomment this <div role="tabpanel" class="tab-pane" id="tab3">
                                    <div class="row">
                                        <?php  foreach ($lunches as $key => $lunch) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$lunch['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $lunch['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $lunch['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price"> -->
                                                                <!-- <p>RM <?php echo  $lunch['Menuitem']['price'];?></p> -->
                                                                <!-- <p>$<?php echo  $lunch['Menuitem']['price'];?></p> -->
                                                   <!--    uncomment this  </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div> -->
                                <!-- // End tab 3// -->
                                 <!-- tab 4 -->
                                <!-- uncomment this  <div role="tabpanel" class="tab-pane" id="tab4"> 
                                    <div class="row">
                                        <?php  foreach ($dinners as $key => $dinner) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$dinner['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $dinner['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $dinner['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $dinner['Menuitem']['price'];?></p> -->
                                                                <!-- <p>$<?php echo  $dinner['Menuitem']['price'];?></p> -->
                                                               <!-- uncomment this </div>  
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div> -->
                                <!-- // End tab 4// -->
                                           
                        <!--  </div>  uncomment this 
                    </div>  -->

                                    <!-- // End My testing// -->


                                    <!-- <div role="tabpanel" class="tab-pane active" id="tab1">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="fooder-menu-description float_left">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/01.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Puttu and Kadala Curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/2.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Dosa with Sambar and Chutney</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/3.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Idli Sambar/ Chutney</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/4.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Appam and Stew</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/5.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Idiyappam and Egg curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/6.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"> Ela Sadya</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="fooder-menu-description float_right">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/7.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Spicy Chicken fry </a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/8.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Kerala Style Prawn Curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/9.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Pumpkin and Lentil Stew </a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/10.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Kerala Style Fish Molee</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/11.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Kerala Style beef </a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/12.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Thalaserry Biryani</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div role="tabpanel" class="tab-pane" id="tab2">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="fooder-menu-description float_left">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/4.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Appam And Stew</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/3.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Idli Sambar/ Chutney</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/2.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Dosa With Sambar And Chutney</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/01.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Puttu And Kadala Curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-6 col-sm-6 col-xs-12"> -->
                                                <!-- <div class="fooder-menu-description float_right">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/5.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Idiyappam And Egg Curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div> -->
                                                    <!-- <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/7.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Fried Potatoes With Garlic</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div> -->
                                                    <!-- <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/5.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Fried Potatoes With Garlic</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div> -->
                                                    <!-- <div class="single-food-item">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/6.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Fried Potatoes With Garlic</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div> -->
                                                <!-- </div>
                                            </div> -->
                                            
                                        <!-- </div>
                                    </div> -->
                                    
                                    
                                    
                                    
                              <!--       
                                </div>  uncomment this 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--Food menu section end-->

         <!--choose us start-->
        <div class="choose-us">
            <div class="choose-us-top pt-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-title white_bg mb-50 text-center">
                                <h2>Why Choose Us ?</h2>
                                <!-- <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi.</p> -->
                                <!-- <p> The secret of success in life is to eat what you like and let the food fight it out inside,We offer the best Kerala food for an affordable price range.</p> -->
                                <div class="title-2 cd-headline clip wow" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                            <h4>We provide
                                            <span class="cd-words-wrapper">
                                                <b class="is-visible"> Healthy</b>
                                                <b>Quality</b>
                                                <b>Tasty</b>
                                            </span>
                                            food
                                            </h4>
                                            <p> The secret of success in life is to eat what you like and let the food fight it out inside,We offer the best Kerala food for an affordable price range.</p>
                                        </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="choose-use-img">
                                <img src="<?php echo $this->webroot;?>assets/images/choose/choose.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <div class="choose-us-desc grey-bg text-center">
               <!-- <div class="container"> -->
                   <div class="row">
                       <div class="col-md-4 col-sm-4 col-xs-12">
                           <div class="single-choose">
                               <a href="#"><i class="fa fa-globe"></i></a>
                               <h5>clean environment</h5>
                               <p>We are keeping very Clean Enviorment for our customers</p>
                           </div>
                       </div>
                       <div class="col-md-4 col-sm-4 col-xs-12">
                           <div class="single-choose">
                               <a href="#"><i class="mdi mdi-silverware-variant"></i></a>
                              <h5>Tasty Food</h5>
                               <p> We will serve you with the mouth watering dishes.</p>
                           </div>
                       </div>
                       <div class="col-md-4 col-sm-4 col-xs-12">
                           <div class="single-choose">
                               <a href="#"><i class="fa fa-gift"></i></a>
                                <h5>Good service</h5>
                               <p>Providing All kind of service as a resturant.</p>
                               
                           </div>
                       </div>
                   </div>
               </div>
           </div>
        </div>
        <!--choose us end-->
       <!--reservation section start-->
        <div class="reservation ptb-100 white-bg">
            <div class="bg-img-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-title white_bg mb-50 text-center">
                                <h2 class="mb-50">Make A Reservation</h2>
                                <!-- <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi.</p> -->
                            </div>
                        </div>
                    </div>
<script type="text/javascript">
$(document).ready(function(){
var error1 = $('.alert-danger');
$('#reservation').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message class
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"customername" : {required : true },
"customeremail" : {required: true,email: true},
"phone" : {required : true,
           number: true,
           minlength:10,
           maxlength:10
            },
"nofperson" : {required : true, number: true },
"revdate" : {required : true }
// "data[Image][name]" : {required : true, accept: "jpg|jpeg|png|ico|bmp" },
// "data[Image][status]" : {required : true }
},
messages:{
"customername" : {required :"Please enter Name."},
"customeremail" : {required : "Please enter a valid email address",
                  minlength : "Please enter a valid email address"},
"phone" : {required :"Please enter phone no."},
"nofperson" : {required :"Please enter No Of Person."},
"revdate" : {required :"Select a date for reservation."}
// "data[Image][name]" : {required :"Please select image.",accept: "Choose a valid file format."},
// "data[Image][status]" : {required :"Please enter status."}
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-valid').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-valid').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-valid').removeClass('has-error'); // set success class to the control group
label
.closest('.form-valid').removeClass('error');
},
});
});
</script>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="reserv-section-inner">
                                <div class="bg-img"></div>
                                <div class="reserve-form">
                                    <h3 class="reserv-title mb-60">Reservation Form</h3>
                                   
                                    <form name="reservation" id="reservation" method="post">
                                        <div class="reserve-form-top">
                                           
                                         <div class="form-valid"><input type="text" placeholder="Full name" name="customername" id="customername"></div>
                                         <div class="form-valid"><input type="text" placeholder="Email address" name="customeremail" id="customeremail"></div>
                                         <div class="form-valid"><input type="text" placeholder="Mobile number" name="phone" id="phone"></div>
                                         <div class="form-valid"><input type="text" placeholder="Number Of Persons" name="nofperson"  id="nofperson"></div>
                                         <!-- <div class="form-valid"><input type="datetime-local" data-format="dd-mm-yyyy" name="revdate"  id="revdate"></div> -->
                                        <div class="date-time"> 
                                            <div class="form-valid"> 
                                                <div class="input-group date form_datetime"  data-date-format="dd-mm-yyyy - HH:ii p"  data-link-field="dtp_input1"> 
                                                    <input type="text" size="21"  class="" name="revdate" placeholder="Select date and time"  readonly> 
                                                    <span class="input-group-btn"> 
                                                        <button class="btn default date-set" type="button"> 
                                                            <i class="fa fa-calendar"></i> 
                                                        </button> 
                                                    </span> 
                                                </div> 
                                                <input type="hidden" id="dtp_input1" value="" /><br/> 
                                            </div> 
                                        </div> 
                                        <div class="form-valid"> 
                                            <div class="g-recaptcha" data-sitekey="6LevhJQUAAAAAMD_iUXHpoY64DaQ9OTQtpdDxTMp"></div> 
                                        </div>
                                        <div class="reserve-submit mt-40">
                                            <button type="submit">
                                                submit
                                            </button>
                                        </div>
                                    </div>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>

        <!--reservation section end-->

        <!--Our gallery start-->
        <!-- <div class="our-gallery">
            <div class="bg-img-2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-title grey_bg mb-50 text-center">
                                <h2 class="mb-50">our gallery</h2> -->
                                <!-- <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi.</p> -->
                           <!--  </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"> -->
                            <!-- <div class="gallery-menu fix text-center mb-60">
                                <ul>
                                    <li class="active" data-filter="*">all</li>
                                    <li data-filter=".desert">desert</li>
                                    <li data-filter=".coffee">coffee</li>
                                    <li data-filter=".drink">drink</li>
                                    <li data-filter=".catering">catering services</li>
                                </ul>
                            </div> -->
                            <!-- <div class="gallery-item-box row">
                            <?php  foreach ($images as $key => $image) { ?>
                                <div class="col-md-4 col-sm-6 col-xs-12 gallery-item desert drink mb-30">
                                    <div class="single-item-gallery">
                                        <a href="<?php echo $this->webroot.$ImagelargePath.$image['Gallery']['imgname'];?>"alt="">
                                            <span class="plus"><i class="mdi mdi-plus"></i></span>
                                        </a>
                                        <img src="<?php echo $this->webroot.$ImagesmallPath.$image['Gallery']['imgname'];?>" alt="">
                                    </div>
                                </div>
                            <?php }?> -->
                                <!-- <div class="col-md-4 col-sm-6 col-xs-12 gallery-item coffee catering mb-30">
                                    <div class="single-item-gallery">
                                        <a href="<?php echo $this->webroot;?>assets/images/gallery/2.jpg">
                                            <span class="plus"><i class="mdi mdi-plus"></i></span>
                                        </a>
                                        <img src="<?php echo $this->webroot;?>assets/images/gallery/2.jpg" alt="">
                                    </div>
                                </div> 
                                <div class="col-md-4 col-sm-6 col-xs-12 gallery-item desert drink mb-30">
                                    <div class="single-item-gallery">
                                        <a href="<?php echo $this->webroot;?>assets/images/gallery/3.jpg">
                                            <span class="plus"><i class="mdi mdi-plus"></i></span>
                                        </a>    
                                        <img src="<?php echo $this->webroot;?>assets/images/gallery/3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 gallery-item coffee catering mb-30">
                                    <div class="single-item-gallery">
                                        <a href="<?php echo $this->webroot;?>assets/images/gallery/3.jpg">
                                            <span class="plus"><i class="mdi mdi-plus"></i></span>
                                        </a>
                                        <img src="<?php echo $this->webroot;?>assets/images/gallery/4.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 gallery-item coffee drink catering mb-30">
                                    <div class="single-item-gallery">
                                        <a href="<?php echo $this->webroot;?>assets/images/gallery/5.jpg">
                                            <span class="plus"><i class="mdi mdi-plus"></i></span>
                                        </a>
                                        <img src="<?php echo $this->webroot;?>assets/images/gallery/5.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 gallery-item desert catering mb-30">
                                    <div class="single-item-gallery">
                                        <a href="<?php echo $this->webroot;?>assets/images/gallery/6.jpg">
                                            <span class="plus"><i class="mdi mdi-plus"></i></span>
                                        </a>
                                        <img src="<?php echo $this->webroot;?>assets/images/gallery/6.jpg" alt="">
                                    </div>
                                </div> -->
                            <!--     
                            </div>
                        </div> 
                        <div class="read-more">
                      <a href="<?php echo $this->webroot;?>Pages/gallery">View More</a>
                     </div>  
                    </div>
                     
                </div>
            </div>
        </div> -->
        <!-- Our gallery end -->
        <!--our blog start-->
        <!-- <div class="our-blog grey-bg ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title grey_bg mb-50 text-center">
                            <h2 class="mb-50">Our blog</h2>
                            <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi.</p>
                        </div>
                    </div>
                </div>
                <div class="blog-details">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/10.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>21<span>Oct</span></p>
                                    </div>
                                    <div class="blog-title">
                                        <h3><<a href="<?php echo $this->webroot;?>/Pages/blog_details">Sea Food Fest</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog blog-video">
                                <div class="blog-img">
                                    <div class="blog-thumbnail">
                                        <img src="<?php echo $this->webroot;?>assets/images/blog/2.jpg" alt="">
                                    </div>
                                    <div class="blog-hover">
                                        <a href="https://www.youtube.com/watch?v=oxPgDogVFnQ"><i class="mdi mdi-play"></i></a>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 hidden-sm col-xs-12">
                            <div class="single-blog">
                                <div class="blog-thumbnail">
                                    <img src="<?php echo $this->webroot;?>assets/images/blog/3.jpg" alt="">
                                </div>
                                <div class="blog-desc">
                                    <div class="publish-date">
                                        <p>21<span>Oct</span></p>
                                    </div>
                                    <div class="blog-title">
                                        <h3><<a href="<?php echo $this->webroot;?>/Pages/blog_details">Sea Food Fest </a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--our blog end-->

        <!--Tstimonial section start-->
        <div class="testimonial ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title white_bg mb-50 text-center">
                            <h2 class="mb-50">Our Client Loves</h2>
                            <!-- <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi.</p> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="testimonial-sliders">
                            <div class="row">
                                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                                    <div class="testimonial-image-slider text-center">
                                        <div class="sin-testiImage">
                                            <img src="<?php echo $this->webroot;?>assets/images/testimonial/1.jpg" alt="testimonial 1" />
                                        </div>
                                        <div class="sin-testiImage">
                                            <img src="<?php echo $this->webroot;?>assets/images/testimonial/2.jpg" alt="testimonial 1" />
                                        </div>
                                        <div class="sin-testiImage">
                                            <img src="<?php echo $this->webroot;?>assets/images/testimonial/3.jpg" alt="testimonial 1" />
                                        </div>
                                        <!-- <div class="sin-testiImage">
                                            <img src="<?php echo $this->webroot;?>assets/images/testimonial/2.jpg" alt="testimonial 1" />
                                        </div> -->

                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="testimonial-text-slider text-center mt-30">
                                        <div class="single-test-text">
                                            <p class="text-qoute">
                                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                            </p>
                                            <p class="test-text">Good Kerala Food As it is a newly opened Kerala restaurant in the heart of KL , I decided to try it out as i am foodie and love to try out good food. The food was tasty . I had dined here twice with my family and both the times it was perfect. My daughter loved the paruppu shakara payasam .

                                                I would like to recommend this restaurant to all of you ... You will not regret..... Enjoy..lah. </p>
                                            <div class="test-title mb-50">
                                                <h4>Jacob Philip</h4>
                                                <p>Customer</p>
                                            </div>
                                        </div>
                                        <div class="single-test-text">
                                            <p class="text-qoute">
                                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                            </p>
                                            <p class="test-text">Good taste</p>
                                            <div class="test-title mb-50">
                                                <h4>Karthikeyan Jayabalan</h4>
                                                <p>Customer</p>
                                            </div>
                                        </div>
                                        <div class="single-test-text">
                                            <p class="text-qoute">
                                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                            </p>
                                            <p class="test-text">A small, humble and unassuming restaurant in KL state serving very authentic Kerala food and dishes. The chicken biryani is very delicious and fragrant and spiced well, served with delicious pickles and raita. The vegetable Thali meal has wonderful side dishes reminiscent of real food served in Kerala. Both are highly recommended. They also serve other local Kerala favorites like Puttu and many other delicious dishes. Masala tea is wonderful here also. Highly recommended place to visit. If parking is an issue, park at the nearby palm court appartmant.</p>
                                            <div class="test-title mb-50">
                                                <h4>Stephen Abraham</h4>
                                                <p>Customer</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <!--Tstimonial section end-->

        <!--Team brand start-->
       <!--  <div class="team-brand ptb-100 grey-bg">
            <div class="container">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12">
                                <div class="team-list">
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-1.png" alt=""></a>
                                    </div>
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-2.png" alt=""></a>
                                    </div>
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-3.png" alt=""></a>
                                    </div>
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-4.png" alt=""></a>
                                    </div>
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-1.png" alt=""></a>
                                    </div>
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-2.png" alt=""></a>
                                    </div>
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-3.png" alt=""></a>
                                    </div>
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-4.png" alt=""></a>
                                    </div>
                                    <div class="single-team">
                                        <a href="#"><img src="<?php echo $this->webroot;?>assets/images/logo/team-3.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--Team brand end-->
         <!--Fun factor Start-->
         <div class="fun-factor ptb-100 text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="single-fun-factor">
                            <div class="fun-icon">
                                <a href="#"><i class="mdi mdi-silverware"></i></a>
                                <h2 class="count"><span class="counter">100</span>+</h2>
                                <h5>Menu Items</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="single-fun-factor">
                            <div class="fun-icon">
                                <a href="#"><i class="mdi mdi-emoticon-happy"></i></a>
                                 <h2 class="count"><span class="counter">100</span>+</h2>
                                <h5>Visitor Everyday</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="single-fun-factor">
                            <div class="fun-icon">
                                <a href="#"><i class="mdi mdi-human-male-female"></i></a>
                                 <h2 class="count"><span class="counter">500</span>+</h2>
                                <h5>Happy Customers</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 hidden-sm col-xs-12">
                        <div class="single-fun-factor">
                            <div class="fun-icon">
                                <a href="#"><i class="fa fa-heart"></i></a>
                                 <h2 class="count"><span class="counter">100</span>%</h2>
                                <h5>Taste & Love</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fun factor end-->
        <!--Offer section start-->
        <div class="offer-section fix">
            <div class="bg-img ptb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="offer-inner text-center">
                                <h2 class="ttle1">
                                    50% Off 
                                </h2>
                                <h2 class="ttle2">
                                    our Vegetable Soup
                                </h2>
                                <!-- <p>  Lorem ipsum  Newaz dolor sit amet, consectetur adipiscing elit. Donec aliquet dolor libero, eget loved dost venenatis maurisfinibus dictumss. Vestibulum quis elit eget neque porttitor  no amet dolor. Proin pretium purus a lorem  
                           obortis pulvinar. Integer laoreet mi id eros porta euismod. Suspendisse potenti. Nulla eros  </p> -->
                           <p>Vegetable soup is prepared using vegetables and leaf vegetables as main ingredients. Some fruits can also be used, such as tomato, squash, and others.Vegetable soup can be prepared as a stock- or cream-based soup. Basic ingredients in addition to vegetables can include beans, tofu, noodles and pasta, vegetable broth or stock, milk, cream, water, olive or vegetable oil, seasonings, salt and pepper, among myriad others</p>
                                <div class="order-now">
                                    <a href="#">order now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Offer section end-->
        
<style type="text/css">


.cd-words-wrapper{
    color:#e54c2a;
}

span.counter {
    color: #383838;
    font-family: "Lato",sans-serif;
    font-size: 42px;
    font-weight: bold;
    margin-top: 30px;
}

h2.count {
    color: #383838;
    font-family: "Lato",sans-serif;
    font-size: 42px;
    font-weight: bold;
    margin-top: 30px;
}

.our-gallery .read-more > a {
    background: #e54c2a none repeat scroll 0 0;
    color: #fff;
    display: inline-block;
    font-weight: 600;
    padding: 9px 38px;
    text-transform: uppercase;
}

.our-gallery .read-more > a:hover {
    background: #535353;
}

@media (max-width: 767px)
{
span.counter {
    font-size: 30px;
    margin-top: 15px;
}

h2.count {
    font-size: 30px;
    margin-top: 15px;
}
}

</style>
