<!--Breadcrubs start-->
        <div class="breadcrubs">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcurbs-inner text-center">
                            <h3 class="breadcrubs-title">
                                Our Food Menu
                            </h3>
                            <ul>
                                <li><a href="<?php echo $this->webroot;?>">home <span>//</span>  </a></li>
                                <li>Our Food Menu</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--Breadcrubs end-->
       
       <!-- Our Food Menu start -->
       <div class="food-menu white-bg ptb-100">
            <div class="container">
                <div class="row">
                    <div class="food-item-tab home-page">
                        <div class="col-md-12">
                            <div class="foode-item-box fix mb-60 ">
                                <!-- foode-item-box fix mb-90 -->
                                <ul class="nav" role="tablist">
                                    <li role="presentation" class="active"><a aria-controls="tab1" href="#tab1"  data-toggle="tab">All item</a></li>
                                    <li role="presentation"><a href="#tab2" aria-controls="tab2" data-toggle="tab">Breads</a></li>
                                    <li role="presentation"><a href="#tab3" aria-controls="tab3" data-toggle="tab">Chicken delicious</a></li>
                                    <li role="presentation"><a href="#tab4" aria-controls="tab4" data-toggle="tab">Drinks & Deserts</a></li>
                                    <li role="presentation"><a href="#tab5" aria-controls="tab5" data-toggle="tab">Egg Varieties</a></li>
                                    <li role="presentation"><a href="#tab6" aria-controls="tab6" data-toggle="tab">Malabar Biriyani</a></li>
                                    <li role="presentation"><a href="#tab7" aria-controls="tab7" data-toggle="tab">Mutton Specials</a></li>
                                    <li role="presentation"><a href="#tab8" aria-controls="tab8" data-toggle="tab">Rice Corner</a></li>
                                    <li role="presentation"><a href="#tab9" aria-controls="tab9" data-toggle="tab">Starters</a></li>
                                    <li role="presentation"><a href="#tab10" aria-controls="tab10" data-toggle="tab">Seafood</a></li>
                                    <li role="presentation"><a href="#tab11" aria-controls="tab11" data-toggle="tab">Veg</a></li>
                                    <li role="presentation"><a href="#tab12" aria-controls="tab12" data-toggle="tab">Weekend</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="food-item-desc">
                            <div class="col-md-12">
                                <div class="tab-content">

                                 <!-- My Testing -->
                                  <!-- tab1 -->
                                <div role="tabpanel" class="tab-pane active" id="tab1">
                                        <div class="row">
                                        <?php  foreach ($allmenuitems as $key => $allmenuitem) { ?>
        
    
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$allmenuitem['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $allmenuitem['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $allmenuitem['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $allmenuitem['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $allmenuitem['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            <?php  } ?>          
                                        </div>  
                                     </div> 
                                    <!-- // End tab1// -->
                                    <!-- tab 2 -->
                                 <div role="tabpanel" class="tab-pane" id="tab2">
                                    <div class="row">
                                        <?php  foreach ($breads as $key => $bread) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$bread['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $bread['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $bread['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $bread['Menuitem']['price'];?></p>
                                                                <!-- &#8377;<p>$<?php echo  $bread['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                <!-- // End tab 2// -->  
                                <!-- tab 3 -->
                                 <div role="tabpanel" class="tab-pane" id="tab3">
                                    <div class="row">
                                        <?php  foreach ($delicious as $key => $deliciou) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$deliciou['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $deliciou['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $deliciou['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $deliciou['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $deliciou['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                <!-- // End tab 3// -->
                                <!-- tab 4 -->
                                 <div role="tabpanel" class="tab-pane" id="tab4">
                                    <div class="row">
                                        <?php  foreach ($drinks as $key => $drink) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$drink['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $drink['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $drink['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $drink['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $drink['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                 <!-- // End tab 4// -->
                                <!-- tab 5 -->
                                 <div role="tabpanel" class="tab-pane" id="tab5">
                                    <div class="row">
                                        <?php  foreach ($eggvarieties as $key => $eggvariety) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$eggvariety['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $eggvariety['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $eggvariety['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $eggvariety['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $eggvariety['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                <!-- // End tab 5// -->
                                <!--  tab 6 -->
                                <div role="tabpanel" class="tab-pane" id="tab6">
                                    <div class="row">
                                        <?php  foreach ($malabars as $key => $malabar) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$malabar['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $malabar['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $malabar['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $malabar['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $malabar['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                <!-- // End tab 6// -->
                                 <!--  tab 7 -->
                                <div role="tabpanel" class="tab-pane" id="tab7">
                                    <div class="row">
                                        <?php  foreach ($mspecials as $key => $mspecial) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$mspecial['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $mspecial['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $mspecial['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $mspecial['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $mspecial['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                <!-- // End tab 7// -->
                                 <!--  tab 8 -->
                                <div role="tabpanel" class="tab-pane" id="tab8">
                                    <div class="row">
                                        <?php  foreach ($rcorners as $key => $rcorner) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$rcorner['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $rcorner['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $rcorner['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $rcorner['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $rcorner['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                <!-- // End tab 8// -->
                                 <!--  tab 9 -->
                                <div role="tabpanel" class="tab-pane" id="tab9">
                                    <div class="row">
                                        <?php  foreach ($starters as $key => $starter) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$starter['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $starter['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $starter['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $starter['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $starter['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                <!-- // End tab 9// -->
                                 <!--  tab 10 -->
                                <div role="tabpanel" class="tab-pane" id="tab10">
                                    <div class="row">
                                        <?php  foreach ($seafoods as $key => $seafood) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$seafood['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $seafood['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $seafood['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $seafood['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $seafood['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                 <!-- // End tab 10// -->

                                 <!--  tab 11 -->
                                <div role="tabpanel" class="tab-pane" id="tab11">
                                    <div class="row">
                                        <?php  foreach ($vcorners as $key => $vcorner) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$vcorner['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $vcorner['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $vcorner['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $vcorner['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $vcorner['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                               
                                <!-- // End tab 11// -->

                                 <!--  tab 12 -->
                                <div role="tabpanel" class="tab-pane" id="tab12">
                                    <div class="row">
                                        <?php  foreach ($weekends as $key => $weekend) { ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 float_left">
                                                <div class="fooder-menu-description ">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot.$ImagemenuPath.$weekend['Menuitem']['imgname'];?>" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"><?php echo $weekend['Menuitem']['name'];?></a></h2>
                                                                    <p><?php echo $weekend['Menuitem']['details'];?></p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                <p>RM <?php echo  $weekend['Menuitem']['price'];?></p>
                                                                <!-- <p>$<?php echo  $weekend['Menuitem']['price'];?></p> -->
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        <?php  } ?>   
                                    </div>
                                </div>
                                <!-- // End tab 12// -->
                         </div>  
                    </div> 
                                    <!-- // End My testing// -->
                                    <!-- <div role="tabpanel" class="tab-pane active" id="tab1">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="fooder-menu-description float_left">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/01.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Puttu and Kadala Curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/2.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Dosa with Sambar and Chutney</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/3.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Idli Sambar/ Chutney</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/4.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Appam and Stew</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/5.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Idiyappam and Egg curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/6.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#"> Ela Sadya</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="fooder-menu-description float_right">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/7.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Spicy Chicken fry </a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/8.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Kerala Style Prawn Curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/9.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Pumpkin and Lentil Stew </a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/10.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Kerala Style Fish Molee</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/11.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Kerala Style beef </a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/12.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Thalaserry Biryani</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div role="tabpanel" class="tab-pane" id="tab2">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="fooder-menu-description float_left">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/4.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Appam And Stew</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/3.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Idli Sambar/ Chutney</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/2.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Dosa With Sambar And Chutney</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="single-food-item">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/01.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Puttu And Kadala Curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-6 col-sm-6 col-xs-12"> -->
                                                <!-- <div class="fooder-menu-description float_right">
                                                    <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/5.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Idiyappam And Egg Curry</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div> -->
                                                    <!-- <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/7.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Fried Potatoes With Garlic</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div> -->
                                                    <!-- <div class="single-food-item mb-30">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/5.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Fried Potatoes With Garlic</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div> -->
                                                    <!-- <div class="single-food-item">
                                                        <div class="single-food-inner">
                                                            <div class="food-img">
                                                                <img src="<?php echo $this->webroot;?>assets/images/food/6.png" alt="">
                                                            </div>
                                                            <div class="single-food-item-desc">
                                                                <div class="single-food-item-title">
                                                                    <h2><a href="#">Fried Potatoes With Garlic</a></h2>
                                                                    <p>Mushroom / Garlic / Veggies</p>
                                                                </div>
                                                                <div class="single-food-price">
                                                                    <p>$100</p>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div> -->
                                                <!-- </div>
                                            </div> -->
                                            
                                        <!-- </div>
                                    </div> -->
                                    
                                    
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
       <!-- Our Food Menu End -->
       <style type="text/css">
.foode-item-box:before {
    /*background: url(images/food/bg-1.png)no-repeat scroll left center;*/
    background: none!important;
    left: 0;
}
.foode-item-box:after {
    /*background: url(images/food/bg-2.png)no-repeat scroll right center;*/
    background: none!important;
    right: 0;
}



@media (min-width: 992px){
.foode-item-box ul li a {
    
    padding: 35px 15px!important;
  
}
.foode-item-box {
   
    height: 190px!important;
    
}
}
@media (max-width: 767px){
  .foode-item-box {
   
    height: 190px!important;
    
}  
.foode-item-box ul li a {
    
    padding: 18px 5px!important;
  
} 
}
@media (max-width: 480px){
  .foode-item-box {
   
    height: 290px!important;
    
} 

}
       </style>