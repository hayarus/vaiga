-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2019 at 07:37 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vaiga`
--

-- --------------------------------------------------------

--
-- Table structure for table `dg_menucategories`
--

CREATE TABLE `dg_menucategories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dg_menucategories`
--

INSERT INTO `dg_menucategories` (`id`, `name`, `status`, `created`, `modified`) VALUES
(1, 'Breads', 1, '0000-00-00 00:00:00', '2019-01-02 07:49:22'),
(2, 'Chicken Delicious', 1, '0000-00-00 00:00:00', '2019-01-02 07:01:40'),
(3, 'Drinks & Deserts', 1, '0000-00-00 00:00:00', '2019-01-02 07:02:09'),
(4, 'Egg Varieties', 1, '2019-01-02 06:46:52', '2019-01-02 07:02:45'),
(5, 'Malabar Biriyani', 1, '2019-01-02 07:03:14', '2019-01-02 07:03:14'),
(6, 'Mutton Specials', 1, '2019-01-02 07:03:41', '2019-01-02 07:03:41'),
(7, 'Rice Corner', 1, '2019-01-02 07:04:11', '2019-01-02 07:04:11'),
(8, 'Starters', 1, '2019-01-02 07:04:39', '2019-01-02 07:04:39'),
(9, 'Seafood Delicious', 1, '2019-01-02 07:05:15', '2019-01-02 07:05:15'),
(10, 'Veg Corner', 1, '2019-01-02 07:05:52', '2019-01-02 07:05:52'),
(11, 'Weekend Specials', 1, '2019-01-02 07:06:12', '2019-01-02 07:06:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dg_menucategories`
--
ALTER TABLE `dg_menucategories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dg_menucategories`
--
ALTER TABLE `dg_menucategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
