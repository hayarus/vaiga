-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2019 at 11:07 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vaiga`
--

-- --------------------------------------------------------

--
-- Table structure for table `dg_menuitems`
--

CREATE TABLE `dg_menuitems` (
  `id` int(11) NOT NULL,
  `menucategory_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `imgname` varchar(100) NOT NULL,
  `details` varchar(200) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dg_menuitems`
--

INSERT INTO `dg_menuitems` (`id`, `menucategory_id`, `name`, `imgname`, `details`, `price`, `created`, `modified`) VALUES
(16, 1, 'Puttu ( Steam Rice Cake)', 'img-8900146-01.png', 'Puttu ', '2.00', '2018-11-02 12:06:52', '2019-01-02 10:53:17'),
(17, 1, 'Dosa ', 'img-5492248-2.png', 'Dosa ', '1.50', '2018-11-02 12:13:24', '2019-01-02 10:52:23'),
(18, 1, 'Iddly (set)', 'img-7711486-3.png', 'Iddly ', '2.00', '2018-11-02 12:14:47', '2019-01-02 10:51:56'),
(25, 1, 'Palappam', 'img-4703369-appam.png', 'Appom', '1.50', '2018-11-05 06:38:44', '2019-01-02 10:48:23'),
(26, 1, 'Erachi Puttu Mutton', 'img-6152648-erachiputtu.jpg', 'Erachi Puttu Mutton\r\n', '9.00', '2018-11-05 06:40:47', '2019-01-03 16:01:20'),
(27, 1, 'Kerala Parotta', 'img-9550170-keralaparotta.png', 'Kerala Parotta', '2.00', '2019-01-02 10:47:04', '2019-01-02 10:47:04'),
(28, 1, 'Erachi Puttu Chicken', 'img-3831481-erachiputtu.png', 'Erachi Puttu Chicken\r\n\r\n', '7.00', '2019-01-02 11:06:40', '2019-01-03 16:01:36'),
(29, 5, 'Mutton Biriyani', 'img-499877-mutton.png', 'Mutton Biriyani', '15.00', '2019-01-02 11:37:09', '2019-01-02 11:37:09'),
(30, 5, 'Chicken Biriyani', 'img-9816589-chickenbiriyani.png', 'Chicken Biriyani', '12.00', '2019-01-02 11:42:25', '2019-01-02 11:42:25'),
(31, 5, 'Veg Biriyani', 'img-8430175-vegbiriyani.png', 'Veg Biriyani', '8.00', '2019-01-02 11:48:03', '2019-01-02 11:48:03'),
(32, 5, 'Prawn Biriyani', 'img-2548522-prawnbiriyani.png', 'Prawn Biriyani', '15.00', '2019-01-02 11:52:18', '2019-01-02 11:52:18'),
(33, 5, 'Fish Biriyani', 'img-6406555-fishbiriyani.png', 'Fish Biriyani', '15.00', '2019-01-02 11:58:39', '2019-01-02 11:58:39'),
(34, 5, 'Egg Biriyani', 'img-5218200-eggbiriyani.png', 'Egg Biriyani', '8.00', '2019-01-02 12:04:51', '2019-01-02 12:04:51'),
(35, 10, 'Sambar', 'img-9582519-sambar.png', 'Sambar', '4.00', '2019-01-02 12:21:50', '2019-01-02 12:21:50'),
(36, 10, 'Veg Kuruma', 'img-462646-vegkuruma.png', 'Veg Kuruma', '6.00', '2019-01-02 12:24:40', '2019-01-02 12:24:40'),
(37, 10, 'Aloo Gopi', 'img-308532-aloogobi.jpg', 'Aloo Gopi', '6.00', '2019-01-02 12:34:12', '2019-01-02 12:34:12'),
(38, 10, 'Chilli Gobi', 'img-2012023-chilligobi.png', 'Chilli Gobi', '6.00', '2019-01-02 12:40:03', '2019-01-02 12:40:03'),
(39, 10, 'Chana Masala', 'img-9171752-chanamasala.png', 'Chana Masala', '6.00', '2019-01-02 12:44:14', '2019-01-02 12:44:14'),
(40, 10, 'Green Peas Masala', 'img-1186828-Greenpea.png', 'Green Peas Masala', '6.00', '2019-01-02 18:06:54', '2019-01-02 18:06:54'),
(41, 10, 'Bitter Gourd Fry', 'img-1725463-bittergourd.png', 'Bitter Gourd Fry', '6.00', '2019-01-02 18:34:15', '2019-01-02 18:34:15'),
(42, 10, 'Aloo Chilli Fry', 'img-7582702-aloochilli.png', 'Aloo Chilli Fry', '8.00', '2019-01-02 18:41:10', '2019-01-02 18:41:10'),
(43, 10, 'Bhindi Masala', 'img-4264526-bhindimasala.png', 'Bhindi Masala', '8.00', '2019-01-02 18:50:48', '2019-01-02 18:50:48'),
(44, 10, 'Bhindi Fry', 'img-2663269-bhindifry.png', 'Bhindi Fry', '6.00', '2019-01-02 19:04:46', '2019-01-02 19:04:46'),
(45, 10, 'Panner Butter Masala', 'img-1741027-PannerButterMasala.jpg', 'Panner Butter Masala', '10.00', '2019-01-02 19:09:22', '2019-01-02 19:09:22'),
(46, 10, 'Mashroom Masala', 'img-1120910-mushroom.png', 'Mashroom Masala', '8.00', '2019-01-02 19:14:30', '2019-01-02 19:14:30'),
(47, 10, 'Mushroom Pepper Fry', 'img-8383178-mushroomfry.png', 'Mushroom Pepper Fry', '10.00', '2019-01-02 19:21:36', '2019-01-02 19:21:36'),
(48, 10, 'Mushroom Chilli Fry', 'img-5905456-Mushroomchillifry.png', 'Mushroom Chilli Fry', '10.00', '2019-01-02 19:29:35', '2019-01-02 19:29:35'),
(49, 10, 'Kadala Curry', 'img-8290405-kadalacuryy.png', 'Kadala Curry', '6.00', '2019-01-02 19:35:21', '2019-01-02 19:35:21'),
(50, 11, 'Kappa Biriyani Chicken', 'img-6468811-kappabiriyanichicken.png', 'Tapioca,Chicken etc', '8.00', '2019-01-03 05:42:42', '2019-01-03 05:42:42'),
(51, 11, 'Kappa Biriyani Mutton', 'img-6600341-kappabiriyanimutton.png', 'Kappa Biriyani Mutton', '10.00', '2019-01-03 05:44:05', '2019-01-03 05:44:05'),
(52, 11, 'Plain Kappa', 'img-8829650-Plain-Kappa.png', 'Boiled Tapioca', '5.00', '2019-01-03 05:52:27', '2019-01-03 05:52:27'),
(53, 11, 'Ellum Kappa', 'img-6013183-ellumkappa.png', 'Tapioca', '8.00', '2019-01-03 05:58:13', '2019-01-03 05:58:13'),
(54, 11, 'Thattu Dosa', 'img-574340-thattudosa.png', 'Dosa', '2.00', '2019-01-03 06:17:07', '2019-01-03 06:17:07'),
(55, 4, 'Egg Omlate', 'img-4808044-eggomlette.png', 'Egg', '3.00', '2019-01-03 09:42:01', '2019-01-03 09:42:01'),
(56, 4, 'Egg Roast', 'img-4875488-eggroast.png', 'Egg Roast', '6.00', '2019-01-03 09:44:17', '2019-01-03 09:44:17'),
(57, 4, 'Egg Masala', 'img-8945617-eggcurry.png', 'Egg', '6.00', '2019-01-03 09:44:50', '2019-01-03 09:44:50'),
(58, 4, 'Egg Burgi (Scrambled Egg)', 'img-1720581-eggbul.png', 'Egg Burgi', '4.00', '2019-01-03 09:45:43', '2019-01-03 09:45:43'),
(59, 6, 'Mutton Roast', 'img-1528320-muttonroast.png', 'Mutton Roast', '10.00', '2019-01-03 10:35:58', '2019-01-03 10:35:58'),
(60, 6, 'Mutton Stew', 'img-6668090-muttonstew.png', 'Mutton Stew', '10.00', '2019-01-03 10:36:40', '2019-01-03 10:36:40'),
(61, 6, 'Mutton Malabari', 'img-9026794-muttonmalabar.png', 'Mutton Malabari', '10.00', '2019-01-03 10:37:36', '2019-01-03 10:37:36'),
(62, 6, 'Mutton Vindaloo', 'img-4933471-muttonvindallo.png', 'Mutton Vindaloo', '10.00', '2019-01-03 10:39:42', '2019-01-03 10:39:42'),
(63, 6, 'Mutton Pepper Fry', 'img-3078308-muttonpepper.png', 'Mutton Pepper Fry', '10.00', '2019-01-03 10:42:04', '2019-01-03 10:42:04'),
(64, 2, 'Kerala Chicken Curry', 'img-8086547-keralachicken.png', 'Kerala Chicken Curry', '6.00', '2019-01-03 11:44:43', '2019-01-03 11:45:00'),
(65, 2, 'Chicken Varuval', 'img-7388610-chickenvaruval.png', 'Chicken Varuval', '8.00', '2019-01-03 11:49:17', '2019-01-03 11:49:17'),
(66, 2, 'Chicken Varatharachathu', 'img-7854309-varutharachathu.png', 'Chicken Varatharachathu', '10.00', '2019-01-03 11:51:59', '2019-01-03 11:51:59'),
(67, 2, 'Chicken Stew', 'img-4730834-chickenstew.png', 'Chicken Stew', '10.00', '2019-01-03 11:55:20', '2019-01-03 11:55:20'),
(68, 2, 'Chicken Roast', 'img-4740905-chickenroast.png', 'Chicken Roast', '10.00', '2019-01-03 11:57:32', '2019-01-03 11:57:32'),
(69, 2, 'Kadai Chicken', 'img-4907226-kadaichicken.png', 'Kadai Chicken', '10.00', '2019-01-03 12:00:36', '2019-01-03 12:00:36'),
(70, 2, 'Chicken 65', 'img-6959228-chicken65.png', 'Chicken 65', '10.00', '2019-01-03 12:03:42', '2019-01-03 12:03:42'),
(71, 2, 'Pepper Chicken', 'img-4198303-pepperchicken.png', 'Pepper Chicken', '10.00', '2019-01-03 12:09:06', '2019-01-03 12:09:06'),
(72, 2, 'Chilli Chicken', 'img-6155090-chillichicken.png', 'Chilli Chicken', '10.00', '2019-01-03 12:11:47', '2019-01-03 12:11:47'),
(73, 2, 'Garlic Chicken', 'img-3101196-garlicchicken.png', 'Garlic Chicken', '10.00', '2019-01-03 12:17:03', '2019-01-03 12:17:03'),
(74, 2, 'Butter Chicken', 'img-6485290-butterchicken.png', 'Butter Chicken', '10.00', '2019-01-03 12:20:57', '2019-01-03 12:20:57'),
(75, 2, 'Kerala Chicken Fry', 'img-7873229-keralachickenfry.png', 'Kerala Chicken Fry', '6.00', '2019-01-03 12:26:55', '2019-01-03 12:26:55'),
(76, 7, 'Thali Meal (Veg)', 'img-6932373-thalimeal.png', 'Thali Meal', '8.00', '2019-01-03 12:43:50', '2019-01-03 12:49:32'),
(77, 7, 'Thali Meal (Chicken)', 'img-4605712-thalimeal.png', 'Thali Meal Chicken', '12.00', '2019-01-03 12:49:09', '2019-01-03 12:50:01'),
(78, 7, 'Thali Meal(Fish)', 'img-8945312-thalifish.png', 'Thali Meal(Fish)', '12.00', '2019-01-03 12:52:16', '2019-01-03 12:52:39'),
(79, 7, 'Thali Meal (Mutton)', 'img-4719238-thalimutton.png', 'Thali Meal (Mutton)', '13.00', '2019-01-03 12:55:52', '2019-01-03 12:55:52'),
(80, 7, 'Indian Rice', 'img-8806457-indianrice.png', 'Indian Rice', '2.00', '2019-01-03 12:57:45', '2019-01-03 12:57:45'),
(81, 7, 'Basmathi Rice', 'img-1015014-basmathi.png', 'Basmathi Rice', '3.00', '2019-01-03 12:59:32', '2019-01-03 12:59:32'),
(82, 7, 'Biriyani Rice', 'img-2216491-biriyanirice.png', 'Biriyani Rice', '4.00', '2019-01-03 13:01:16', '2019-01-03 13:01:16'),
(83, 7, 'Jeera rice', 'img-9515991-jeera.png', 'Jeera rice', '6.00', '2019-01-03 13:03:12', '2019-01-03 13:03:12'),
(84, 7, 'Lemon Rice', 'img-1914367-lemon.png', 'Lemon Rice', '6.00', '2019-01-03 13:05:08', '2019-01-03 13:05:08'),
(85, 7, 'Tomato Rice', 'img-8527221-tomato.png', 'Tomato Rice', '6.00', '2019-01-03 13:11:49', '2019-01-03 13:11:49'),
(86, 7, 'Curd Rice', 'img-6322937-curdrice.png', 'Curd Rice', '6.00', '2019-01-03 13:14:37', '2019-01-03 13:14:37'),
(87, 7, 'Veg Pulav', 'img-4271240-vegpulav.png', 'Veg Pulav', '7.00', '2019-01-03 13:20:15', '2019-01-03 13:20:15'),
(88, 7, 'Veg Fried Rice', 'img-149841-vegfriedrice.png', 'Veg Fried Rice', '6.00', '2019-01-03 18:31:58', '2019-01-03 18:31:58'),
(89, 7, 'Chicken Fried Rice', 'img-2131347-ChickenFriedRice.png', 'Chicken Fried Rice', '7.00', '2019-01-03 18:34:58', '2019-01-03 18:34:58'),
(90, 7, 'Seafood Fried Rice', 'img-4834289-Seafoodfriedrice.png', 'Seafood Fried Rice', '10.00', '2019-01-03 18:37:44', '2019-01-03 18:37:44'),
(91, 7, 'Veg Noodle', 'img-1798706-vegnoodles.png', 'Veg Noodle', '8.00', '2019-01-03 18:40:04', '2019-01-03 18:40:04'),
(92, 7, 'Chicken Noodle', 'img-6388854-Chickennoodle.png', 'Chicken Noodle', '10.00', '2019-01-03 18:43:07', '2019-01-03 18:43:07'),
(93, 8, 'Chicken Soup', 'img-6943969-chickensoup.png', 'Chicken Soup', '4.00', '2019-01-03 18:46:20', '2019-01-03 18:46:20'),
(94, 8, 'Mushroom Soup', 'img-2808837-mushroomsoup.png', 'Mushroom Soup', '4.00', '2019-01-03 18:48:51', '2019-01-03 18:48:51'),
(95, 8, 'Mutton Soup', 'img-7847595-muttonsoup.png', 'Mutton Soup', '5.00', '2019-01-03 18:50:51', '2019-01-03 18:50:51'),
(96, 8, 'Onion Pakoda', 'img-6378173-onionpakoda.png', 'Onion Pakoda', '4.00', '2019-01-03 18:54:01', '2019-01-03 18:54:01'),
(97, 8, 'Chicken Pakoda', 'img-9936523-ChickenPakoda.png', 'Chicken Pakoda', '6.00', '2019-01-03 18:56:23', '2019-01-03 18:56:23'),
(98, 8, 'Kothu Parotta', 'img-9348449-kotthparotta.png', 'Kothu Parotta', '6.00', '2019-01-03 18:59:24', '2019-01-03 18:59:24'),
(99, 8, 'Chilli Parotta', 'img-6805725-chilliparotta.png', 'Chilli Parotta', '5.00', '2019-01-03 19:01:39', '2019-01-03 19:01:39'),
(100, 8, 'Veg Salad', 'img-2310180-vegsalad.png', 'Veg Salad', '5.00', '2019-01-03 19:03:11', '2019-01-03 19:03:11'),
(101, 3, 'kaayal special juice', 'img-6633300-lassi.png', 'kaayal special juice', '4.00', '2019-01-04 09:23:46', '2019-01-04 09:45:32'),
(102, 3, 'Mango juice', 'img-4540100-mango.png', 'Mango juice', '4.00', '2019-01-04 09:24:22', '2019-01-04 09:24:22'),
(103, 3, 'Apple juice', 'img-9746704-img-1424865-apple.png', 'Apple juice', '4.00', '2019-01-04 09:24:51', '2019-01-04 10:49:15'),
(104, 3, 'Water Melon juice', 'img-6081237-watermelon.png', 'Water Melon juice', '4.00', '2019-01-04 09:25:36', '2019-01-04 10:51:57'),
(105, 3, 'lime jucie', 'img-7373962-mintlime.png', 'lime jucie', '3.00', '2019-01-04 09:26:06', '2019-01-04 09:26:06'),
(106, 3, 'Mint Lime Juice', 'img-4165039-mintlime.png', 'Mint Lime Juice', '4.00', '2019-01-04 09:27:13', '2019-01-04 10:53:40'),
(107, 3, 'Soda Lime Juice', 'img-6069641-sodalime.png', 'Soda Lime Juice', '4.00', '2019-01-04 09:28:00', '2019-01-04 09:28:00'),
(108, 3, 'Lassi(Sweet/Salt)', 'img-3952026-lassi.png', 'Lassi(Sweet/Salt)', '4.00', '2019-01-04 09:28:39', '2019-01-04 09:28:39'),
(109, 3, 'Mango Lessi', 'img-3073120-mango.png', 'Mango Lessi', '6.00', '2019-01-04 09:29:14', '2019-01-04 09:29:14'),
(110, 3, 'Tea Hot', 'img-5256652-hottea.png', 'Tea Hot', '2.00', '2019-01-04 09:29:50', '2019-01-04 10:54:22'),
(111, 3, 'Tea ice', 'img-9122314-teaice.png', 'Tea ice', '3.00', '2019-01-04 09:30:28', '2019-01-04 09:30:28'),
(112, 3, 'Tea Hot Fresh Milk', 'img-1586303-tea.png', 'Tea Hot Fresh Milk', '3.00', '2019-01-04 09:31:06', '2019-01-04 10:59:23'),
(113, 3, 'Necafe hot', 'img-6918334-necafehot.png', 'Necafe hot', '3.00', '2019-01-04 09:31:49', '2019-01-04 11:01:09'),
(114, 3, 'nefscafe ice', 'img-3178405-nice.png', 'nefscafe ice', '4.00', '2019-01-04 09:32:36', '2019-01-04 11:04:14'),
(115, 3, 'Masala Tea', 'img-8791809-masalatea.png', 'Masala Tea', '3.00', '2019-01-04 09:33:13', '2019-01-04 11:00:02'),
(116, 3, 'Bru Coffee', 'img-796508-brucoffe.png', 'Bru Coffee', '3.00', '2019-01-04 09:36:13', '2019-01-04 09:36:13'),
(117, 3, 'Milohot', 'img-7725219-milohot.png', 'Milohot', '2.00', '2019-01-04 09:36:39', '2019-01-04 09:36:39'),
(118, 3, 'Miloice', 'img-3003845-miloice.png', 'Miloice', '3.00', '2019-01-04 09:37:10', '2019-01-04 09:37:10'),
(119, 3, 'Fresh Milk', 'img-5274353-hotmilk.png', 'Fresh Milk', '3.00', '2019-01-04 09:38:46', '2019-01-04 09:38:46'),
(120, 3, 'Lemon Tea Hot', 'img-6953125-lemonhottea.png', 'Lemon Tea Hot', '2.00', '2019-01-04 09:39:52', '2019-01-04 09:39:52'),
(121, 3, 'Lemon Tea Ice', 'img-1658020-lemonteaice.png', 'Lemon Tea Ice', '3.00', '2019-01-04 09:40:19', '2019-01-04 09:40:19'),
(122, 3, 'Black Coffee', 'img-1967773-blackcoffee.png', 'Black Coffee', '2.00', '2019-01-04 09:40:58', '2019-01-04 09:40:58'),
(123, 3, 'Black Tea', 'img-1835327-blacktea.png', 'Black Tea', '2.00', '2019-01-04 09:41:27', '2019-01-04 09:41:27'),
(124, 3, 'ChukkuCoffee', 'img-1630554-chukkucoffee.png', 'ChukkuCoffee', '3.00', '2019-01-04 09:42:01', '2019-01-04 09:42:01'),
(125, 3, 'Butter Milk', 'img-404052-hotmilk.png', 'Butter Milk', '4.00', '2019-01-04 09:42:42', '2019-01-04 09:42:42'),
(126, 3, '100 plus/coke/sprite', 'img-754699-100plus.png', '100 plus/coke/sprite', '2.00', '2019-01-04 09:43:19', '2019-01-04 09:43:19'),
(127, 3, 'paayasam', 'img-5934143-payasam.png', 'paayasam', '4.00', '2019-01-04 09:43:57', '2019-01-04 09:43:57'),
(128, 9, 'Fish Molly', 'img-810546-fiahmolly.png', 'Fish Molly', '10.00', '2019-01-04 10:11:31', '2019-01-04 10:11:31'),
(129, 9, 'ChilliFish', 'img-3557739-chillifish.png', 'ChilliFish', '10.00', '2019-01-04 10:11:59', '2019-01-04 10:11:59'),
(130, 9, 'FishMasala', 'img-5873413-fishmasala.png', 'FishMasala', '10.00', '2019-01-04 10:12:31', '2019-01-04 10:12:31'),
(131, 9, 'Kottayam FishCurry', 'img-4281921-kottayam.png', 'Kottayam FishCurry', '8.00', '2019-01-04 10:13:07', '2019-01-04 10:13:07'),
(132, 9, 'Malabar Fish Curry', 'img-2498474-malabar.png', 'Malabar Fish Curry', '8.00', '2019-01-04 10:13:38', '2019-01-04 10:13:38'),
(133, 9, 'Fish Varatharachathu', 'img-5632324-meenavaratharachath.png', 'Fish Varatharachathu', '10.00', '2019-01-04 10:14:54', '2019-01-04 10:14:54'),
(134, 9, 'Mango Fish Curry', 'img-3940734-mangofish.png', 'Mango Fish Curry', '10.00', '2019-01-04 10:15:27', '2019-01-04 10:15:27'),
(135, 9, 'Fish Pollichathu', 'img-1901245-pollichathu.png', 'Fish Pollichathu', '0.00', '2019-01-04 10:16:23', '2019-01-04 10:16:23'),
(136, 9, 'Special Fish Fry', 'img-1903076-specialfishfry.png', 'Fish Pollichathu', '0.00', '2019-01-04 10:17:05', '2019-01-04 10:17:05'),
(137, 9, 'Fish Pollichathu', 'img-1827697-prawnmasala.png', 'Fish Pollichathu', '10.00', '2019-01-04 10:17:44', '2019-01-04 10:17:44'),
(138, 9, 'Prawn Ulathiythu', 'img-5065612-prawnularthiyathu.png', 'Prawn Ulathiythu', '10.00', '2019-01-04 10:18:57', '2019-01-04 10:18:57'),
(139, 9, 'Crab', 'img-8455200-crab.png', 'Crab', '0.00', '2019-01-04 10:19:35', '2019-01-04 10:19:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dg_menuitems`
--
ALTER TABLE `dg_menuitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menucategory_id` (`menucategory_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dg_menuitems`
--
ALTER TABLE `dg_menuitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dg_menuitems`
--
ALTER TABLE `dg_menuitems`
  ADD CONSTRAINT `dg_menuitems_ibfk_1` FOREIGN KEY (`menucategory_id`) REFERENCES `dg_menucategories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
