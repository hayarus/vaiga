-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2019 at 08:59 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vaiga`
--

-- --------------------------------------------------------

--
-- Table structure for table `dg_offercoupons`
--

CREATE TABLE `dg_offercoupons` (
  `id` int(11) NOT NULL,
  `coupon` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dg_offercoupons`
--

INSERT INTO `dg_offercoupons` (`id`, `coupon`, `name`, `email`, `phone`, `created`, `modified`) VALUES
(1, 'C - 18347', 'jishnu', 'jishnu@gmail.com', 7356816358, '2019-05-08 07:12:31', '2019-05-08 07:33:00'),
(2, 'C - 12753', 'jishnu', 'jishnu@yahoo.in', 7356816358, '2019-05-08 17:24:30', '2019-05-08 17:24:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dg_offercoupons`
--
ALTER TABLE `dg_offercoupons`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dg_offercoupons`
--
ALTER TABLE `dg_offercoupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
