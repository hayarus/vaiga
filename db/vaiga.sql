-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2018 at 11:13 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vaiga`
--

-- --------------------------------------------------------

--
-- Table structure for table `dg_galleries`
--

CREATE TABLE `dg_galleries` (
  `id` int(11) NOT NULL,
  `imgname` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dg_galleries`
--

INSERT INTO `dg_galleries` (`id`, `imgname`, `status`, `created`, `modified`) VALUES
(1, 'img-831604-2.jpg', 1, '2018-11-03 06:19:12', '2018-11-08 06:50:53'),
(2, 'img-9155578-6.jpg', 1, '2018-11-03 06:43:33', '2018-11-03 06:44:45'),
(3, 'img-1768188-3.jpg', 1, '2018-11-03 10:43:30', '2018-11-03 10:43:30'),
(4, 'img-5034790-4.jpg', 1, '2018-11-03 10:43:41', '2018-11-03 10:43:41'),
(5, 'img-177917-5.jpg', 1, '2018-11-03 10:43:50', '2018-11-03 10:43:50'),
(6, 'img-3687438-g-7.jpg', 0, '2018-11-03 10:44:02', '2018-11-03 10:53:17'),
(7, 'img-3447570-g-9.jpg', 1, '2018-11-03 10:44:14', '2018-11-03 10:44:14'),
(8, 'img-8174438-6.jpg', 1, '2018-11-07 06:28:49', '2018-11-07 06:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `dg_menuitems`
--

CREATE TABLE `dg_menuitems` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `imgname` varchar(100) NOT NULL,
  `details` varchar(200) NOT NULL,
  `category` int(11) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dg_menuitems`
--

INSERT INTO `dg_menuitems` (`id`, `name`, `imgname`, `details`, `category`, `price`, `created`, `modified`) VALUES
(16, 'Puttu and kadala Curry', 'img-8900146-01.png', 'Puttu and kadala Curry', 1, '55.00', '2018-11-02 12:06:52', '2018-11-08 06:50:18'),
(17, 'Dosa And Chatney', 'img-5492248-2.png', 'Dosa And Chatney', 1, '65.00', '2018-11-02 12:13:24', '2018-11-03 04:44:41'),
(18, 'Iddly And Chatney', 'img-7711486-3.png', 'Iddly And Chatney', 1, '40.00', '2018-11-02 12:14:47', '2018-11-03 04:44:00'),
(20, 'Ela sadhya', 'img-7014160-6.png', 'Ela sadhya', 2, '90.00', '2018-11-03 09:07:42', '2018-11-03 09:07:42'),
(25, 'Appom And Stew', 'img-4703369-appam.png', 'Appom And Stew', 1, '45.00', '2018-11-05 06:38:44', '2018-11-05 06:45:03'),
(26, 'Appom And Stew', 'img-7575988-appam.png', 'Appom And Stew', 3, '40.00', '2018-11-05 06:40:47', '2018-11-05 06:45:36');

-- --------------------------------------------------------

--
-- Table structure for table `dg_reservations`
--

CREATE TABLE `dg_reservations` (
  `id` int(11) NOT NULL,
  `customername` varchar(100) NOT NULL,
  `revdate` datetime NOT NULL,
  `nofperson` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dg_reservations`
--

INSERT INTO `dg_reservations` (`id`, `customername`, `revdate`, `nofperson`, `created`, `modified`) VALUES
(1, 's1', '0000-00-00 00:00:00', 0, '2018-11-04 16:39:56', '2018-11-04 16:39:56'),
(2, 's1', '0000-00-00 00:00:00', 1, '2018-11-04 16:43:57', '2018-11-04 16:43:57'),
(3, 's1', '0000-00-00 00:00:00', 1, '2018-11-04 16:45:02', '2018-11-04 16:45:02'),
(4, 's1', '0000-00-00 00:00:00', 1, '2018-11-04 16:45:38', '2018-11-04 16:45:39'),
(5, 'sww', '0000-00-00 00:00:00', 1, '2018-11-04 16:59:25', '2018-11-04 16:59:25'),
(6, 'swwa', '1970-01-01 01:00:00', 1, '2018-11-04 17:08:34', '2018-11-04 17:08:34'),
(7, 'swwa', '2013-02-12 11:11:00', 3, '2018-11-04 17:09:52', '2018-11-04 17:09:52'),
(8, 'jishnu', '2019-02-20 10:10:00', 1, '2018-11-05 10:42:02', '2018-11-05 10:42:02'),
(9, 'Jishnu', '2018-11-15 10:00:00', 1, '2018-11-08 09:39:56', '2018-11-08 09:39:56'),
(10, 'Jishnu', '2018-11-10 03:10:00', 1, '2018-11-08 09:45:32', '2018-11-08 09:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `dg_users`
--

CREATE TABLE `dg_users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dg_users`
--

INSERT INTO `dg_users` (`id`, `name`, `email`, `password`, `created`, `modified`) VALUES
(3, 'admin', 'admin@admin.com', '76be26b6a2d7ae3e4f5ebd905a8ebc56ba1e4d9e', '2018-11-03 18:46:23', '2018-11-03 18:46:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dg_galleries`
--
ALTER TABLE `dg_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dg_menuitems`
--
ALTER TABLE `dg_menuitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dg_reservations`
--
ALTER TABLE `dg_reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dg_users`
--
ALTER TABLE `dg_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dg_galleries`
--
ALTER TABLE `dg_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `dg_menuitems`
--
ALTER TABLE `dg_menuitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `dg_reservations`
--
ALTER TABLE `dg_reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `dg_users`
--
ALTER TABLE `dg_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
